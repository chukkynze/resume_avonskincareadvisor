http://vm.avon-scd-api.local/api/v1/init-scd-api?devKey=b699bf8eadbc24d5c74322c783bb47f9&marketId=174&repPubKey=1e5bc474b25dbd2d40b2b6bf2c99089f


TRUNCATE avondb.markets_config_scdtool;
INSERT INTO avondb.markets_config_scdtool 
(id, market_id, products_api_feed_url, dsi_dept_search_term, use_dsi_feed, edit_dsi_feed_products, num_addtodb_products_selectable, created_at, updated_at, facebook_login, site_login, registration, forgot_password, registration_url, forgot_password_url) 
VALUES 
(1, 174, 'https://www.avon.com/hyfn/hyfn_products_en-dev.json', '', 1, 0, 5, null, null, 1, 0, 1, 0, 'https://www.avon.com/?overlay=RegistrationModal&emailsignup=0', 'https://www.avon.com/?overlay=ForgottenPassword&emailsignup=0');
