## Avon Skin Care Diagnostic Tool API

Avon Skincare Diagnostic - https://www.avon.com/skinadvisor

* Developed API for Avon's Skin Care Advisor tool at using Laravel 5.0 in 2 months
* I also designed the Backend CMS features used to configure the Skin Advisor tool in their CodeIgniter backed CMS.

### Installation

#### Step 1: Install Homestead
Documentation for Homestead can be found on the [Laravel Homestead website](http://laravel.com/docs/5.0/homestead#installation-and-setup).

#### Step 2: Clone Repo
`git clone git@github.com:hyfn/avon-scd-api.git`

#### Step 3: Update Your Environment
Update the values in `.env.example` with your own local config values.

#### Step 4: Update Your Hosts File
Follow the instructions [here](http://www.rackspace.com/knowledge_center/article/how-do-i-modify-my-hosts-file)

#### Step 5: Update Your Homestead Settings
Follow the instructions at the [Laravel Homestead website](http://laravel.com/docs/5.0/homestead#installation-and-setup)

#### Step 6: Start Homestead and The API
* Enter `homestead up` in any directory in your terminal and, if you set up Homestead properly and with global rights, your virtual box should spin up with the Avon SCD Tool API ready to go at the domain you specified in your .env & hosts file.
* You can ssh into the box with `homestead ssh`
* You can "setup" your dev environment for the app to with `bash deploy.sh local` at the root of the project.
* You can "destroy" bring down Homestead with `homestead destroy` at any directory in your terminal.


### API Routes

#### Initializing the API tool
* http://vm.avon-scd-api.local/api/v1/init-scd-api?devKey=devkey&langCode=langCode&countryCode=countryCode

This will return this JSON:
`
STATUS 200 OK
TIME 809 ms
Pretty Raw Preview  JSON Copy
{
    "avonSCDToolAPI_Response": 
    {
        "data": 
        {
            "quiz": {
"simple": {
"1": {
"questionText": "",
"answerOptions": {
"1": {
"answerText": "",
"products": {
"1": {
"id": 1,
"name": "",
"description": "",
"price": "",
"price_exceptions": "",
"line_number": "",
"marketing_copy": {
"results_page": {
"field1": "stuff1",
"field2": "stuff2",
"field3": "stuff3"
}
}
}
}
},
"2": "",
"3": "",
"4": "",
"5": ""
}
}
},
"comprehensive": [ ]
}
},
"responseType": "success",
"requestAction": "init-scd-api",
"version": "v1",
"statusCode": 200,
"timestamp": 1429560066,
"datetime": "2015-04-20T20:01:06"
}
}
`