#!/usr/bin/env bash

# Usage : sudo bash deploy.sh local

if [ $1 = 'local' ]; then

    # Composer Stuff
    composer install
    composer update
    composer dump-autoload

    # XDebug Stuff
	sudo echo '\n ' 							>> /etc/php5/fpm/conf.d/20-xdebug.ini
	sudo echo 'xdebug.remote_port=9000' 		>> /etc/php5/fpm/conf.d/20-xdebug.ini
	sudo echo 'xdebug.remote_mode=req' 		>> /etc/php5/fpm/conf.d/20-xdebug.ini
	sudo echo 'xdebug.remote_host=127.0.0.1'	>> /etc/php5/fpm/conf.d/20-xdebug.ini
	sudo echo 'xdebug.remote_handler=dbgp' 	>> /etc/php5/fpm/conf.d/20-xdebug.ini
	sudo echo 'xdebug.remote_connect_back=1' >> /etc/php5/fpm/conf.d/20-xdebug.ini
	sudo echo 'xdebug.remote_enable=1' 		>> /etc/php5/fpm/conf.d/20-xdebug.ini
	sudo echo 'xdebug.remote_autostart=0' 	>> /etc/php5/fpm/conf.d/20-xdebug.ini
	sudo echo 'xdebug.max_nesting_level=400' >> /etc/php5/fpm/conf.d/20-xdebug.ini
	sudo echo 'xdebug.idekey=PHPSTORM' 		>> /etc/php5/fpm/conf.d/20-xdebug.ini
	sudo echo 'xdebug.default_enable=1' 		>> /etc/php5/fpm/conf.d/20-xdebug.ini
	sudo echo 'xdebug.cli_color=1' 			>> /etc/php5/fpm/conf.d/20-xdebug.ini
	sudo echo 'xdebug.scream=0' 				>> /etc/php5/fpm/conf.d/20-xdebug.ini
	sudo echo 'xdebug.show_local_vars=1' 	>> /etc/php5/fpm/conf.d/20-xdebug.ini
	sudo echo '\n ' 							>> /etc/php5/fpm/conf.d/20-xdebug.ini
	echo "------------------------------------------------------------------------------------------------------"
	echo "Enabled XDebug on Http."
	echo "------------------------------------------------------------------------------------------------------"

	sudo echo '\n ' 							>> /etc/php5/cli/php.ini
	sudo echo 'xdebug.remote_port=9000' 		>> /etc/php5/cli/php.ini
	sudo echo 'xdebug.remote_mode=req' 		>> /etc/php5/cli/php.ini
	sudo echo 'xdebug.remote_host=127.0.0.1'	>> /etc/php5/cli/php.ini
	sudo echo 'xdebug.remote_handler=dbgp' 	>> /etc/php5/cli/php.ini
	sudo echo 'xdebug.remote_connect_back=1' >> /etc/php5/cli/php.ini
	sudo echo 'xdebug.remote_enable=1' 		>> /etc/php5/cli/php.ini
	sudo echo 'xdebug.remote_autostart=0' 	>> /etc/php5/cli/php.ini
	sudo echo 'xdebug.max_nesting_level=400' >> /etc/php5/cli/php.ini
	sudo echo 'xdebug.idekey=PHPSTORM' 		>> /etc/php5/cli/php.ini
	sudo echo 'xdebug.default_enable=1' 		>> /etc/php5/cli/php.ini
	sudo echo 'xdebug.cli_color=1' 			>> /etc/php5/cli/php.ini
	sudo echo 'xdebug.scream=0' 				>> /etc/php5/cli/php.ini
	sudo echo 'xdebug.show_local_vars=1' 	>> /etc/php5/cli/php.ini
	sudo echo '\n ' 							>> /etc/php5/cli/php.ini
	echo "------------------------------------------------------------------------------------------------------"
	echo "Enabled XDebug on CLI."
	echo "------------------------------------------------------------------------------------------------------"

	sudo service php5-fpm restart


    # Clockwork Stuff
    php artisan clockwork:clean

    php artisan route:clear
    php artisan route:cache

    php artisan config:clear
    php artisan config:cache

    php artisan cache:clear
    php artisan clear-compiled
    php artisan ide-helper:meta
    php artisan ide-helper:models -W -R
    php artisan ide-helper:generate
    php artisan optimize

elif [ $1 = 'dev' ]; then

    composer install
    composer update
    composer dump-autoload

    php artisan route:clear
    php artisan route:cache

    php artisan config:clear
    php artisan config:cache

    php artisan cache:clear
    php artisan clear-compiled
    php artisan optimize

elif [ $1 = 'stg' ]; then

    composer install
    composer update
    composer dump-autoload

    php artisan route:clear
    php artisan route:cache

    php artisan config:clear
    php artisan config:cache

    php artisan cache:clear
    php artisan clear-compiled
    php artisan optimize

elif [ $1 = 'prod' ]; then

    composer install
    composer update
    composer dump-autoload

    php artisan route:clear
    php artisan route:cache

    php artisan config:clear
    php artisan config:cache

    php artisan cache:clear
    php artisan clear-compiled
    php artisan optimize

elif [ $1 = 'WillsPrius' ]; then

    php artisan inspire

else

    echo "Invalid deployment environment. Valid environments are 'local', 'dev', 'stg', 'prod', and 'WillsPrius'. "

fi