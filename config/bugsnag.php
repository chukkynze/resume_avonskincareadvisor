<?php # config/bugsnag.php

return array(
    'api_key' => env('BUGSNAG_apikey', ''),
);