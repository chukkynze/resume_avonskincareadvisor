<?php

/**
 * Class Hyfn
 * Hyfn specific api calls and services
 */
class Hyfn {

    public function __construct()
    {

    }

    /**
     * Use the hyfn profanity checker to check words for profanities
     *
     * @param $marketId
     * @param $text
     * @return mixed
     */
    public static function profanityCheck($marketId,$text)
    {
        $profanityCheckUrl  =   str_replace('{{market_id}}',$marketId,Config::get('hyfn.profanity_check_url'));
        $request            =   Httpful\Request::get($profanityCheckUrl.'?text='.$text)->send();

        return $request->body;
    }

}