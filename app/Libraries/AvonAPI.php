<?php namespace App\Libraries;

use App\Models\Api\V1\Market;
use App\Models\Api\V1\MarketTranslation;
use App\Models\Api\V1\QuizUser;
use App\Models\Api\V1\QuizResult;
use App\Models\Api\V1\Representative;
use App\Models\Api\V1\ScdConfig;

use Request as CurrentRequest;
use Response;
use Validator;
use Illuminate\Http\Request;
use Exception;

class AvonAPI
{
    /**
     * Check to see if Required params for this API such as devKey and the API version were passed and are valid
     *
     * @author William Wallace <william.wallace@hyfn.com>
     * @author Chukky Nze <chukky.nze@hyfn.com>
     *
     * @param Request $request
     * @return array|bool
     * @throws Exception
     */
    public static function checkRequestInputs(Request $request)
    {
        $requestAction = $request->segment(3);

        if($requestAction != '')
        {
            switch($requestAction)
            {
                case 'init-scd-api'                     :   $rules  =   [
                                                                            'devKey'        =>  'required|apikey',
                                                                            'marketId'      =>  'required|marketid',
                                                                            'parentUrl'     =>  'required',
                                                                            'shopperId'     =>  'numeric',

                                                                            'customerId'    =>  'numeric',
                                                                            'facebookId'    =>  'facebookid',
                                                                            'vkontakteId'   =>  'vkontakteid',
                                                                        ];
                                                            break;
                case 'init-scd-api-rep-id'              :   $rules  =   [
                                                                            'devKey'        =>  'required|apikey',
                                                                            'marketId'      =>  'required|marketid',
                                                                            'repId'         =>  'required|repid',

                                                                            'parentUrl'     =>  'required',
                                                                            'shopperId'     =>  'numeric',

                                                                            'customerId'    =>  'numeric',
                                                                            'facebookId'    =>  'facebookid',
                                                                            'vkontakteId'   =>  'vkontakteid',
                                                                        ];
                                                            break;
                case 'init-scd-api-rep-pubkey'          :   $rules  =   [
                                                                            'devKey'        =>  'required|apikey',
                                                                            'marketId'      =>  'required|marketid',
                                                                            'repPubKey'     =>  'required|pubkey', // repID not needed when repPubKey is passed especially since the pubkey is safer

                                                                            'parentUrl'     =>  'required',
                                                                            'shopperId'     =>  'numeric',

                                                                            'customerId'    =>  'numeric',
                                                                            'facebookId'    =>  'facebookid',
                                                                            'vkontakteId'   =>  'vkontakteid',
                                                                        ];
                                                            break;

                case 'save-quiz-results-customer-id'    :   $rules  =   [
                                                                            'devKey'        =>  'required|apikey',
                                                                            'marketId'      =>  'required|marketid',
                                                                            'repPubKey'     =>  'required|pubkey', // repID not needed when repPubKey is passed especially since the pubkey is safer

                                                                            'parentUrl'     =>  'required|parenturl',

                                                                            'customerId'    =>  'required|numeric',
                                                                            'vkontakteId'   =>  'numeric',
                                                                            'facebookId'    =>  'numeric',

                                                                            'first_name'    =>  'required',
                                                                            //'last_name'     =>  'numeric',
                                                                            'shopperId'     =>  'numeric',
                                                                        ];
                                                            break;

                case 'save-quiz-results-vkontakte-id'   :   $rules  =   [
                                                                            'devKey'        =>  'required|apikey',
                                                                            'marketId'      =>  'required|marketid',
                                                                            'repPubKey'     =>  'required|pubkey', // repID not needed when repPubKey is passed especially since the pubkey is safer

                                                                            'parentUrl'     =>  'required|parenturl',

                                                                            'customerId'    =>  'numeric',
                                                                            'vkontakteId'   =>  'required|numeric',
                                                                            'facebookId'    =>  'numeric',

                                                                            'first_name'    =>  'required',
                                                                            //'last_name'     =>  'numeric',
                                                                            'shopperId'     =>  'numeric',
                                                                        ];
                                                            break;

                case 'save-quiz-results-facebook-id'    :   $rules  =   [
                                                                            'devKey'        =>  'required|apikey',
                                                                            'marketId'      =>  'required|marketid',
                                                                            'repPubKey'     =>  'required|pubkey', // repID not needed when repPubKey is passed especially since the pubkey is safer

                                                                            'parentUrl'     =>  'required|parenturl',

                                                                            'customerId'    =>  'numeric',
                                                                            'vkontakteId'   =>  'numeric',
                                                                            'facebookId'    =>  'required|numeric',

                                                                            'first_name'    =>  'required',
                                                                            //'last_name'     =>  'numeric',
                                                                            'shopperId'     =>  'numeric',
                                                                        ];
                                                            break;

                case 'get-quiz-results-customer-id'    :   $rules  =   [
                                                                            'devKey'        =>  'required|apikey',
                                                                            'marketId'      =>  'required|marketid',
                                                                            'repPubKey'     =>  'required|pubkey', // repID not needed when repPubKey is passed especially since the pubkey is safer

                                                                            'parentUrl'     =>  'required|parenturl',

                                                                            'customerId'    =>  'required|numeric',
                                                                        ];
                                                            break;

                case 'get-quiz-results-vkontakte-id'   :   $rules  =   [
                                                                            'devKey'        =>  'required|apikey',
                                                                            'marketId'      =>  'required|marketid',
                                                                            'repPubKey'     =>  'required|pubkey', // repID not needed when repPubKey is passed especially since the pubkey is safer

                                                                            'parentUrl'     =>  'required|parenturl',

                                                                            'vkontakteId'   =>  'required|numeric',
                                                                        ];
                                                            break;

                case 'get-quiz-results-facebook-id'    :   $rules  =   [
                                                                            'devKey'        =>  'required|apikey',
                                                                            'marketId'      =>  'required|marketid',
                                                                            'repPubKey'     =>  'required|pubkey', // repID not needed when repPubKey is passed especially since the pubkey is safer

                                                                            'parentUrl'     =>  'required|parenturl',

                                                                            'facebookId'    =>  'required|numeric',
                                                                        ];
                                                            break;

                default : return static::makeAvonResponse(['Invalid API Action.'], 501);
            }
        }
        else
        {
            return static::makeAvonResponse(['Empty API Action.'], 501);
        }


        return static::validateRequestAction($rules, $request);
    }


    /**
     * Base Validation
     *
     * @author William Wallace <william.wallace@hyfn.com>
     * @author Chukky Nze <chukky.nze@hyfn.com>
     *
     * @param $rules
     * @param Request $request
     * @return array|bool
     */
    public static function validateRequestAction($rules, Request $request)
    {
        $messages   =   [
                            'required'      =>  json_encode
                                                ([
                                                    'fieldName'     =>  ':attribute',
                                                    'errorMessage'  =>  'This Field Is Required',
                                                    'errorCode'     =>  '100-1'
                                                ]),
                            'integer'       =>  json_encode
                                                ([
                                                    'fieldName'     =>  ':attribute',
                                                    'errorMessage'  =>  'Invalid Value for this field (integer)',
                                                    'errorCode'     =>  '100-2'
                                                ]),
                            'alpha'         =>  json_encode
                                                ([
                                                    'fieldName'     =>  ':attribute',
                                                    'errorMessage'  =>  'Invalid Value for this field (alpha)',
                                                    'errorCode'     =>  '100-3'
                                                ]),
                            'list'          =>  json_encode
                                                ([
                                                    'fieldName'     =>  ':attribute',
                                                    'errorMessage'  =>  'Invalid Value for this field (list)',
                                                    'errorCode'     =>  '100-4'
                                                ]),
                            'email'         =>  json_encode
                                                ([
                                                    'fieldName'     =>  ':attribute',
                                                    'errorMessage'  =>  'Invalid Value for this field (email)',
                                                    'errorCode'     =>  '100-5'
                                                ]),
                            'max'           =>  json_encode
                                                ([
                                                    'fieldName'     =>  ':attribute',
                                                    'errorMessage'  =>  'Invalid Value for this field (too long)',
                                                    'errorCode'     =>  '100-6'
                                                ]),
                            'numeric'       =>  json_encode
                                                ([
                                                    'fieldName'     =>  ':attribute',
                                                    'errorMessage'  =>  'Invalid Value for this field (numeric)',
                                                    'errorCode'     =>  '100-7'
                                                ]),
                            'market_code'   =>  json_encode
                                                ([
                                                    'fieldName'     =>  ':attribute',
                                                    'errorMessage'  =>  'Invalid Market CD. Market Code could not be resolved.',
                                                    'errorCode'     =>  '100-8'
                                                ]),
                            'lang_code'     =>  json_encode
                                                ([
                                                    'fieldName'     =>  ':attribute',
                                                    'errorMessage'  =>  'langCd is either not present in request or invalid.',
                                                    'errorCode'     =>  '100-9'
                                                ]),
                            'min'           =>  json_encode
                                                ([
                                                    'errorMessage'  =>  'At least one field is required',
                                                    'errorCode'     =>  '100-10'
                                                ]),
                            'apikey'        =>  json_encode
                                                ([
                                                    'fieldName'     =>  ':attribute',
                                                    'errorMessage'  =>  'The devKey must be an active, valid api key',
                                                    'errorCode'     =>  '100-11'
                                                ]),
                            'marketid'      =>  json_encode
                                                ([
                                                    'fieldName'     =>  ':attribute',
                                                    'errorMessage'  =>  'The market ID must be active and valid',
                                                    'errorCode'     =>  '100-12'
                                                ]),
                            'pubkey'        =>  json_encode
                                                ([
                                                    'fieldName'     =>  ':attribute',
                                                    'errorMessage'  =>  'The representative\'s pub key must be active and valid',
                                                    'errorCode'     =>  '100-13'
                                                ]),
                            'repid'         =>  json_encode
                                                ([
                                                    'fieldName'     =>  ':attribute',
                                                    'errorMessage'  =>  'The representative\'s username must be active and valid',
                                                    'errorCode'     =>  '100-13'
                                                ]),
                            'parenturl'     =>  json_encode
                                                ([
                                                    'fieldName'     =>  ':attribute',
                                                    'errorMessage'  =>  'The parent url must be valid [remember the protocol - http, ftp, etc]',
                                                    'errorCode'     =>  '100-14'
                                                ]),
                            'shopperid'     =>  json_encode
                                                ([
                                                    'fieldName'     =>  ':attribute',
                                                    'errorMessage'  =>  'The shopper id must be valid.',
                                                    'errorCode'     =>  '100-15'
                                                ]),
                            'customerid'     =>  json_encode
                                                ([
                                                    'fieldName'     =>  ':attribute',
                                                    'errorMessage'  =>  'The customer id must be valid.',
                                                    'errorCode'     =>  '100-16'
                                                ]),

        ];
        $validator  =   Validator::make($request->all(), $rules, $messages);

        if ($validator->fails())
        {
            $messages       =   $validator->messages();
            $errorMessages  =   [];
            foreach($messages->all() as  $message)
            {
                $errorMessages[]   =   json_decode($message);
            }

            return static::makeAvonResponse($errorMessages, 400);
        }
        else
        {
            return  [
                        'status'    =>  true,
                        'request'   =>  $request,
                    ];
        }
    }


    /**
     * Policy logic for accessing the API, given different routes and various forms of rep data points.
     *
     * @author Chukky Nze <chukky.nze@hyfn.com>
     *
     * @param $apiRoute
     * @param $repPubKey
     * @param $repUsername
     * @param $marketID
     *
     * @return array
     */
    public static function getRepData($apiRoute, $repPubKey, $repUsername, $marketID)
    {
        $repOutput=[];

        if($apiRoute == 'init-scd-api')
        {
            $repOutput  =   [];
        }
        elseif($apiRoute == 'init-scd-api-rep-id')
        {
            $repOutput  =   Representative::getRepDataWithRepUsername($repUsername, $marketID);
        }
        elseif($apiRoute == 'init-scd-api-rep-pubkey')
        {
            $repOutput  =   Representative::getRepDataWithRepPubkey($repPubKey, $marketID);
        }
        else
        {
            $repOutput  =   Representative::getRepDataWithRepPubkey($repPubKey, $marketID);
        }

        return $repOutput;
    }


    /**
     * All responses should go through this filter
     *
     * @author Chukky Nze <chukky.nze@hyfn.com>
     *
     * @param array     $data
     * @param int       $code
     * @param string    $format
     * @return string|\Symfony\Component\HttpFoundation\Response
     */
    public static function makeAvonResponse($data, $code=200, $format='json')
    {
        $marketID               =   CurrentRequest::input('marketId');
        $customerID             =   CurrentRequest::input('customerId');
        $repPubKey              =   CurrentRequest::input('repPubKey');
        $repID                  =   CurrentRequest::input('repId');

        $repData                =   self::getRepData(CurrentRequest::segment(3), $repPubKey, $repID, $marketID);



        $QuizUser               =   QuizUser::whereCustomerId($customerID)->first();
        $QuizResults            =   (isset($QuizUser)
                                        ?   [
                                                'message'   =>  'Quiz Results were successfully retrieved with the Customer ID provided - [' . $customerID . ']',
                                                'data'      =>  QuizResult::whereQuizUserId($QuizUser
                                                                    ->getId())
                                                                    ->orderBy('updated_at', 'desc')
                                                                    ->first(),
                                            ]
                                        :   [
                                                'message'   =>  'Quiz Results could not be retrieved with the Customer ID provided - [' . $customerID . ']',
                                                'data'      =>  [],
                                            ]
                                    );

        $areThereQuizResults    =   (isset($QuizResults) && count($QuizResults) >= 1
                                        ?   1
                                        :   0 );

        $addOnData              =   [
                                        'requestAction'         =>  CurrentRequest::segment(3),
                                        'version'               =>  CurrentRequest::segment(2),
                                        'statusCode'            =>  $code,
                                        'shopperData'           =>  [], // Shopper::getShopperData(CurrentRequest::input('shopperId')),
                                        'quizUserData'          =>  QuizUser::getQuizUserData($customerID),
                                        'quizResultsData'       =>  $QuizResults,
                                        'areThereQuizResults'   =>  $areThereQuizResults,
                                        'marketData'            =>  Market::getMarketData($marketID),
                                        'configData'            =>  ScdConfig::getConfigData($marketID),
                                        'repData'               =>  $repData, //Representative::getRepData($repPubKey, $marketID),
                                        'translations'          =>  MarketTranslation::getTranslationsByMarketId($marketID),
                                        'timestamp'             =>  strtotime('now'),
                                        'datetime'              =>  date('Y-m-d\TH:i:s'),
                                    ];
        switch($code)
        {
            case 204:   // deleted a resource
            case 201:   // Saved a resource
            case 200:   $finalData  =   [
                                            'data'          =>  $data,
                                            'responseType'  =>  'success',
                                        ] + $addOnData;
                break;


            case 308:   // Permanent Redirect
            case 307:   // Temporary Redirect
            case 301:   // Moved Permanently
            case 300:   $finalData  =   [
                                            'errorMessage'  =>  'These are not the data you\'re looking for',
                                            'responseType'  =>  'error',
                                        ];
                break;


            case 429:   // Too Many Requests
            case 422:   // Unprocessable Entity
            case 410:   // Gone
            case 404:   // Not Found
            case 403:   // Forbidden
            case 401:   // Unauthorized
            case 400:   $finalData  =   [
                                            'data'          =>  $data,
                                            'errorMessage'  =>  'Bad Request',
                                            'responseType'  =>  'error',
                                        ];
                break;



            case 503:   // Service Unavailable
            case 501:   // Not Implemented
                        $finalData  =   [
                                            'data'          =>  $data,
                                            'errorMessage'  =>  'Internal Server Error',
                                            'responseType'  =>  'error',
                                        ];
                        break;
            case 500:   $finalData  =   [
                                            'data'          =>  $data,
                                            'errorMessage'  =>  'Internal Server Error',
                                            'responseType'  =>  'error',
                                        ];
                break;


            default :   $finalData  =   $data + $addOnData;
        }

        return  ($format === 'json'
                    ?   Response::json(['avonSCDToolAPI_Response' => $finalData ], $code)
                    :   'What is this XML that you speak of?');
    }

}
