<?php
 /**
  * Project  :   avon-scd-api
  * Class    :   ValidationServiceProvider
  *
  * filename:   ValidationServiceProvider.php
  *
  * User     : Chukky Nze <chukky.nze@hyfn.com>
  * Date     : 4/10/15
  * Time     : 5:33 PM
  *
  * Copyright 2015 Hyfn.com All Rights Reserved
  */
namespace App\Libraries\Validation;

use Illuminate\Support\ServiceProvider;

class ValidationServiceProvider extends ServiceProvider
{
    public function register(){}

    public function boot()
    {
        $this->app->validator->resolver(function($translator, $data, $rules, $messages)
        {
            return new CustomValidator($translator, $data, $rules, $messages);
        });
    }
} 