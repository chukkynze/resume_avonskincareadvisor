<?php namespace App\Libraries\Validation;

use DB;

use Illuminate\Validation\Validator;

use App\Models\Api\V1\Country;
use App\Models\Api\V1\Language;
use App\Models\Api\V1\Market;
use App\Models\Api\V1\Representative;

class CustomValidator extends Validator
{

    /**
     * Validate the language code
     * the $devKey should be a valid (is_active) api_key in the api_keys table
     *
     * @author William Wallace <william.wallace@hyfn.com>
     * @author Chukky Nze <chukky.nze@hyfn.com>
     *
     * @param $attribute
     * @param $devKey
     * @param $parameters
     *
     * @return bool
     */
    function validateApikey($attribute, $devKey, $parameters)
    {
        $results = DB::select('select id from api_keys where dev_key = ? and is_active = 1', array($devKey));
        return ( sizeof($results) === 1 ? TRUE : FALSE );
    }


    /**
     * @author William Wallace <william.wallace@hyfn.com>
     * @author Chukky Nze <chukky.nze@hyfn.com>
     *
     * @param $attribute
     * @param $value
     * @param array $parameters Contains lang code
     * @return boolean
     */
    function validateMarketid($attribute,$value,$parameters)
    {
        $status = Market::validateMarketById($value);
        return (!$status ? false : true);
    }


    /**
     * Validate the rep's pub key
     *
     * @author Chukky Nze <chukky.nze@hyfn.com>
     *
     * @param $attribute
     * @param $value
     * @param array $parameters Contains lang code
     * @return boolean
     */
    function validatePubkey($attribute,$value,$parameters)
    {
        $models = Representative::validateRepByPublicKey($value);
        return (empty($models) ? false : true);
    }


    /**
     * Validate the rep's pub key
     *
     * @author Chukky Nze <chukky.nze@hyfn.com>
     *
     * @param $attribute
     * @param $value
     * @param array $parameters Contains lang code
     * @return boolean
     */
    function validateRepid($attribute,$value,$parameters)
    {
        $models = Representative::validateRepById($value);
        return (empty($models) ? false : true);
    }


    /**
     * Validate the parentUrl
     *
     * @author Chukky Nze <chukky.nze@hyfn.com>
     *
     * @param $attribute
     * @param $value
     * @param array $parameters Contains lang code
     * @return boolean
     */
    function validateParenturl($attribute,$value,$parameters)
    {
        $status = filter_var($value, FILTER_VALIDATE_URL);
        return (!$status ? false : true);
    }





    // Needs work

    /**
     * Validate the shopper ID
     *
     * @author Chukky Nze <chukky.nze@hyfn.com>
     *
     * @param $attribute
     * @param $value
     * @param array $parameters Contains lang code
     * @return boolean
     */
    function validateShopperid($attribute,$value,$parameters)
    {
        $status = true; // todo: Shopper::validateShopperId($value);
        return (!$status ? false : true);
    }

    /**
     * Validate the customer Id
     *
     * @author Chukky Nze <chukky.nze@hyfn.com>
     *
     * @param $attribute
     * @param $value
     * @param array $parameters Contains lang code
     * @return boolean
     */
    function validateCustomerid($attribute,$value,$parameters)
    {
        $status = true; // todo: Customer::validateCustomerId($value);
        return (!$status ? false : true);
    }








    // Unused


    /**
     * Validate the country/market code
     *
     * @author Chukky Nze <chukky.nze@hyfn.com>
     *
     * @param $attribute
     * @param $value
     * @param $parameters
     *
     * @return bool
     */
    function validateCountryCode($attribute,$value,$parameters)
    {
        $market = Country::getCountryByCode($value);
        return (empty($market) ? false : true);
    }


    /**
     * Validate the language code
     *
     * @author William Wallace <william.wallace@hyfn.com>
     * @author Chukky Nze <chukky.nze@hyfn.com>
     *
     * @param $attribute
     * @param $value
     * @param $parameters
     *
     * @return bool
     */
    function validateLangCode($attribute,$value,$parameters)
    {
        $lang       =   Language::getLanguageByLangCode($value);
        return (empty($lang) ? false : true);
    }


    /**
     * @author William Wallace <william.wallace@hyfn.com>
     *
     * @param $attribute
     * @param $value
     * @param array $parameters Contains lang code
     * @return boolean
     */
    function validateMarketCode($attribute,$value,$parameters)
    {
        $market = Country::getCountryByCode($value);
        return (empty($market) ? false : true);
    }
}
