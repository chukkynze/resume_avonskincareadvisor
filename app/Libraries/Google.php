<?php

use Illuminate\Support\Facades\Facade;

class Google extends Facade
{

    /**
     * Get latitude and longitude from an address
     *
     * @param $addressPart
     * @return array
     */
    public static function geocodeAddress($addressPart)
    {
        $lat        =   0;
        $long       =   0;
        $url        =   Config::get('google.geocodeUrl').'address='.urlencode($addressPart);
        $response   =   json_decode(\Httpful\Request::get($url)->send());

        if(!empty($response->results[0]))
        {
            $results    =   $response->results[0];
            $lat        =   $results->geometry->location->lat;
            $long       =   $results->geometry->location->lng;
        }

        return ['latitude' => $lat, 'longitude' => $long];
    }

    /**
     * Get zip code and country code from a set of latitude and longitude coordinates
     *
     * @param $lat
     * @param $long
     * @return array
     */
    public static function reverseGeocodeToZip($lat,$long)
    {
        $zip            =   null;
        $countryCode    =   null;
        $url            =   Config::get('google.geocodeUrl').'latlng='.urlencode($lat.','.$long);
        $response       =   json_decode(\Httpful\Request::get($url)->send());

        if(!empty($response->results[0]))
        {
            $results        =   $response->results[0];
            $countryCode    =   $results->address_components[4]->short_name;
            $zip            =   $results->address_components[5]->short_name;
        }

        return ['zip' => $zip,'countyCode' => $countryCode];
    }

}