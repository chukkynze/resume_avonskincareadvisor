<?php
/**
 * Project  : avon-scd-api
 *
 * User     : Chukky Nze <chukky.nze@hyfn.com>
 * Date     : 4/17/15
 * Time     : 11:58 AM
 * 
 * Copyright 2015 Hyfn.com All Rights Reserved
 */

namespace App\Models\Api\V1;


/**
 * App\Models\Api\V1\Quiz
 *
 * @property integer $id 
 * @property integer $market_id 
 * @property boolean $active 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Quiz whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Quiz whereMarketId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Quiz whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Quiz whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Quiz whereUpdatedAt($value)
 */
class Quiz extends BaseApiModel
{
    protected $table        =   'scd_quiz';
    protected $primaryKey   =   'id';
    protected $guarded      =   ['id'];
    protected $fillable     =   [
                                    'market_id',
                                    'active',
                                ];
    protected $dates        =   ['deleted_at'];

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getMarketId()
    {
        return $this->market_id;
    }

    /**
     * @param int $market_id
     */
    public function setMarketId($market_id)
    {
        $this->market_id = $market_id;
    }

    /**
     * @return boolean
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param boolean $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return \Carbon\Carbon
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param \Carbon\Carbon $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return \Carbon\Carbon
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param \Carbon\Carbon $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }






}