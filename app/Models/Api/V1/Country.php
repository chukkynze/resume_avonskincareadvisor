<?php
/**
 * Project  : avon-scd-api
 *
 * User     : Chukky Nze <chukky.nze@hyfn.com>
 * Date     : 4/17/15
 * Time     : 11:56 AM
 *
 * Copyright 2015 Hyfn.com All Rights Reserved
 */

namespace App\Models\Api\V1;

/**
 * App\Models\Api\V1\Country
 *
 * @property integer $id 
 * @property string $code 
 * @property string $english_name 
 * @property string $display_name 
 * @property string $base_url 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @property integer $region_id 
 * @property integer $default_market_id 
 * @property-read \Illuminate\Database\Eloquent\Collection|\Market[] $markets 
 * @property-read \Market $defaultMarket 
 * @property-read \Region $region 
 * @property-read \Illuminate\Database\Eloquent\Collection|\Market[] $countryMarket 
 * @property-read \CountryDomain $domain 
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Country whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Country whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Country whereEnglishName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Country whereDisplayName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Country whereBaseUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Country whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Country whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Country whereRegionId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Country whereDefaultMarketId($value)
 */
class Country extends BaseApiModel
{

    public function markets()
    {
        return $this->hasMany('Market')->with('language');
    }

    public function defaultMarket()
    {
        return $this->belongsTo('Market','default_market_id');
    }

    public function region()
    {
        return $this->belongsTo('Region');
    }

    public function countryMarket()
    {
        return $this->hasMany('Market','country_id')->with('language');
    }

    public function domain()
    {
        return $this->hasOne('CountryDomain');
    }

    public static function getCountryAndMarketsByCountryCode($countryCode)
    {
        return Country::with('markets')->where('code',$countryCode)->first();
    }

    public static function getCountryByCode($countryCode)
    {
        return Country::where('code',$countryCode)->first();
    }

}