<?php
/**
 * Project  : avon-scd-api
 *
 * User     : Chukky Nze <chukky.nze@hyfn.com>
 * Date     : 4/17/15
 * Time     : 11:58 AM
 *
 * Copyright 2015 Hyfn.com All Rights Reserved
 */

namespace App\Models\Api\V1;


/**
 * App\Models\Api\V1\QuizUser
 *
 * @property integer $id 
 * @property integer $market_id 
 * @property integer $customer_id 
 * @property integer $facebook_id 
 * @property integer $vkontakte_id 
 * @property string $first_name 
 * @property string $last_name 
 * @property integer $shopper_id 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\QuizUser whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\QuizUser whereMarketId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\QuizUser whereCustomerId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\QuizUser whereFacebookId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\QuizUser whereVkontakteId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\QuizUser whereFirstName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\QuizUser whereLastName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\QuizUser whereShopperId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\QuizUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\QuizUser whereUpdatedAt($value)
 */
class QuizUser extends BaseApiModel
{
    protected $table        =   'scd_quiz_user';
    protected $primaryKey   =   'id';
    protected $guarded      =   ['id'];
    protected $fillable     =   [
                                    'market_id',

                                    'customer_id',
                                    'facebook_id',
                                    'vkontakte_id',

                                    'first_name',
                                    'last_name',

                                    'shopper_id',
                                ];
    protected $rules        =   [
                                    'market_id'     => 'required|numeric|exists:markets,market_id',

                                    'customer_id'   => 'required_without_all:facebook_id,vkontakte_id',
                                    'facebook_id'   => 'required_without_all:customer_id,vkontakte_id',
                                    'vkontakte_id'  => 'required_without_all:customer_id,facebook_id',

                                    'first_name'    => 'required',
                                    //'last_name'     => '',
                                    'shopper_id'    => 'numeric',
                                ];


    public static function getQuizUserData($customerId)
    {
        $QuizUser   =   self::whereCustomerId($customerId)
                        ->orderBy('updated_at', 'desc')
                        ->first();

        return (isset($QuizUser) ? $QuizUser->toArray() : NULL);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getMarketId()
    {
        return $this->market_id;
    }

    /**
     * @param int $market_id
     */
    public function setMarketId($market_id)
    {
        $this->market_id = $market_id;
    }

    /**
     * @return int
     */
    public function getCustomerId()
    {
        return $this->customer_id;
    }

    /**
     * @param int $customer_id
     */
    public function setCustomerId($customer_id)
    {
        $this->customer_id = $customer_id;
    }

    /**
     * @return int
     */
    public function getFacebookId()
    {
        return $this->facebook_id;
    }

    /**
     * @param int $facebook_id
     */
    public function setFacebookId($facebook_id)
    {
        $this->facebook_id = $facebook_id;
    }

    /**
     * @return int
     */
    public function getVkontakteId()
    {
        return $this->vkontakte_id;
    }

    /**
     * @param int $vkontakte_id
     */
    public function setVkontakteId($vkontakte_id)
    {
        $this->vkontakte_id = $vkontakte_id;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * @param string $first_name
     */
    public function setFirstName($first_name)
    {
        $this->first_name = $first_name;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * @param string $last_name
     */
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;
    }

    /**
     * @return int
     */
    public function getShopperId()
    {
        return $this->shopper_id;
    }

    /**
     * @param int $shopper_id
     */
    public function setShopperId($shopper_id)
    {
        $this->shopper_id = $shopper_id;
    }

    /**
     * @return \Carbon\Carbon
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param \Carbon\Carbon $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return \Carbon\Carbon
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param \Carbon\Carbon $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }


}