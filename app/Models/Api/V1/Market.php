<?php
/**
 * Project  : avon-scd-api
 *
 * User     : Chukky Nze <chukky.nze@hyfn.com>
 * Date     : 4/17/15
 * Time     : 11:56 AM
 *
 * Copyright 2015 Hyfn.com All Rights Reserved
 */

namespace App\Models\Api\V1;

use Cache;
use Schema;

/**
 * App\Models\Api\V1\Market
 *
 * @property integer $market_id 
 * @property string $market_name 
 * @property string $market_name_slug 
 * @property integer $market_mktcd 
 * @property string $market_name_link 
 * @property integer $lang_id 
 * @property string $timezone 
 * @property string $name_fmt 
 * @property string $market_datetime 
 * @property string $short_name 
 * @property integer $country_id 
 * @property boolean $active 
 * @property-read \Country $country 
 * @property-read \Language $language 
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Market whereMarketId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Market whereMarketName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Market whereMarketNameSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Market whereMarketMktcd($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Market whereMarketNameLink($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Market whereLangId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Market whereTimezone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Market whereNameFmt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Market whereMarketDatetime($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Market whereShortName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Market whereCountryId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Market whereActive($value)
 */
class Market extends BaseApiModel
{
    protected $table = 'markets';
    protected $primaryKey = 'market_id';
    public $timestamps = false;

    public function country() {
        return $this->belongsTo('Country');
    }

    public function language(){
        return $this->belongsTo('Language','lang_id');
    }

    public static function getMarketData($marketID)
    {
        if(Schema::hasTable('markets'))
        {
            $output     =   self::whereMarketId($marketID)->where('active', 1)->with('country')->get([
                                'market_name',
                                'market_name_slug',
                                'market_mktcd',
                                'market_name_link',
                                'lang_id',
                                'timezone',
                                'name_fmt',
                                'market_datetime',
                                'short_name',
                                'country_id',
                                'active',
                            ])->toArray();
        }
        else
        {
            $output     =   "The Markets table does not exist and/or is not accessible.";
        }
        return $output;
    }

    public static function getMarketsListByRegionId($regionId)
    {
        $markets = self::getMarketsList('countries.region_id',$regionId)->toArray();
        return $markets;
    }

    public static function getMarketsListByCountryCode($countryCode)
    {
        $markets = self::getMarketsList('countries.code',$countryCode)->toArray();
        return $markets;
    }

    private static function getMarketsList($key,$value){

        $markets = Country::join( 'regions', 'regions.id', '=', 'countries.region_id' )
            ->join( 'markets', 'markets.country_id','=','countries.id')
            ->join( 'languages', 'languages.lang_id', '=', 'markets.lang_id' )
            ->select('countries.code as mrktCd')
            ->addSelect('languages.lang_name as lang_display_name','languages.lang_abbr as langCd')
            ->addSelect('markets.market_id as hyfn_market_id')
            ->addSelect('languages.lang_image as lang_image')
            ->where($key,$value)->get();

        // Is the market config present and is it live
        foreach ($markets as $market)
        {
            $is_live = "0";
            if($config = AppConfig::where('market_id',$market['hyfn_market_id'])->first())
            {
                $is_live = $config->is_live;
            }
            $market['is_live'] = $is_live;
        }

        return $markets;
    }

    public static function validateMarketById($id)
    {
        $object  = self::find($id);
        return (is_object($object) ? true : false);
    }



}
