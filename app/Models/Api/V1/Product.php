<?php
/**
 * Project  : avon-scd-api
 *
 * User     : Chukky Nze <chukky.nze@hyfn.com>
 * Date     : 4/17/15
 * Time     : 11:56 AM
 * 
 * Copyright 2015 Hyfn.com All Rights Reserved
 */

namespace App\Models\Api\V1;

/**
 * App\Models\Api\V1\Product
 *
 * @property integer $id 
 * @property integer $market_id 
 * @property integer $feed_id 
 * @property string $auto_replenishment_caption 
 * @property string $auto_replenishment_frequencies 
 * @property boolean $can_buy 
 * @property string $product_brand 
 * @property string $profile_number 
 * @property string $variant_type 
 * @property string $name 
 * @property string $description 
 * @property string $image 
 * @property boolean $show_price_in_ui 
 * @property boolean $is_from_dsi_feed 
 * @property boolean $is_auto_replenish 
 * @property boolean $is_normal_purchase 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Product whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Product whereMarketId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Product whereFeedId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Product whereAutoReplenishmentCaption($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Product whereAutoReplenishmentFrequencies($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Product whereCanBuy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Product whereProductBrand($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Product whereProfileNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Product whereVariantType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Product whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Product whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Product whereImage($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Product whereShowPriceInUi($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Product whereIsFromDsiFeed($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Product whereIsAutoReplenish($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Product whereIsNormalPurchase($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Product whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Product whereUpdatedAt($value)
 */
class Product extends BaseApiModel
{
    protected $table        =   'scd_products';
    protected $primaryKey   =   'id';
    protected $guarded      =   ['id'];
    protected $fillable     =   [
                                    'market_id',

                                    'feed_id',

                                    'auto_replenishment_caption',
                                    'auto_replenishment_frequencies', // delivery cadence units not durations

                                    'can_buy',

                                    'product_brand', // category
                                    'profile_number',
                                    'variant_type',

                                    'name',
                                    'description',
                                    'image',

                                    'show_price_in_ui',

                                    'is_from_dsi_feed',
                                    'is_auto_replenish',
                                    'is_normal_purchase',
                                ];

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getMarketId()
    {
        return $this->market_id;
    }

    /**
     * @param int $market_id
     */
    public function setMarketId($market_id)
    {
        $this->market_id = $market_id;
    }

    /**
     * @return int
     */
    public function getFeedId()
    {
        return $this->feed_id;
    }

    /**
     * @param int $feed_id
     */
    public function setFeedId($feed_id)
    {
        $this->feed_id = $feed_id;
    }

    /**
     * @return string
     */
    public function getAutoReplenishmentCaption()
    {
        return $this->auto_replenishment_caption;
    }

    /**
     * @param string $auto_replenishment_caption
     */
    public function setAutoReplenishmentCaption($auto_replenishment_caption)
    {
        $this->auto_replenishment_caption = $auto_replenishment_caption;
    }

    /**
     * @return string
     */
    public function getAutoReplenishmentFrequencies()
    {
        return $this->auto_replenishment_frequencies;
    }

    /**
     * @param string $auto_replenishment_frequencies
     */
    public function setAutoReplenishmentFrequencies($auto_replenishment_frequencies)
    {
        $this->auto_replenishment_frequencies = $auto_replenishment_frequencies;
    }

    /**
     * @return boolean
     */
    public function isCanBuy()
    {
        return $this->can_buy;
    }

    /**
     * @param boolean $can_buy
     */
    public function setCanBuy($can_buy)
    {
        $this->can_buy = $can_buy;
    }

    /**
     * @return string
     */
    public function getProductBrand()
    {
        return $this->product_brand;
    }

    /**
     * @param string $product_brand
     */
    public function setProductBrand($product_brand)
    {
        $this->product_brand = $product_brand;
    }

    /**
     * @return string
     */
    public function getProfileNumber()
    {
        return $this->profile_number;
    }

    /**
     * @param string $profile_number
     */
    public function setProfileNumber($profile_number)
    {
        $this->profile_number = $profile_number;
    }

    /**
     * @return string
     */
    public function getVariantType()
    {
        return $this->variant_type;
    }

    /**
     * @param string $variant_type
     */
    public function setVariantType($variant_type)
    {
        $this->variant_type = $variant_type;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return boolean
     */
    public function isShowPriceInUi()
    {
        return $this->show_price_in_ui;
    }

    /**
     * @param boolean $show_price_in_ui
     */
    public function setShowPriceInUi($show_price_in_ui)
    {
        $this->show_price_in_ui = $show_price_in_ui;
    }

    /**
     * @return boolean
     */
    public function isIsFromDsiFeed()
    {
        return $this->is_from_dsi_feed;
    }

    /**
     * @param boolean $is_from_dsi_feed
     */
    public function setIsFromDsiFeed($is_from_dsi_feed)
    {
        $this->is_from_dsi_feed = $is_from_dsi_feed;
    }

    /**
     * @return boolean
     */
    public function isIsAutoReplenish()
    {
        return $this->is_auto_replenish;
    }

    /**
     * @param boolean $is_auto_replenish
     */
    public function setIsAutoReplenish($is_auto_replenish)
    {
        $this->is_auto_replenish = $is_auto_replenish;
    }

    /**
     * @return boolean
     */
    public function isIsNormalPurchase()
    {
        return $this->is_normal_purchase;
    }

    /**
     * @param boolean $is_normal_purchase
     */
    public function setIsNormalPurchase($is_normal_purchase)
    {
        $this->is_normal_purchase = $is_normal_purchase;
    }

    /**
     * @return \Carbon\Carbon
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param \Carbon\Carbon $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return \Carbon\Carbon
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param \Carbon\Carbon $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }


}