<?php
/**
 * Project  : avon-scd-api
 *
 * User     : Chukky Nze <chukky.nze@hyfn.com>
 * Date     : 4/17/15
 * Time     : 11:56 AM
 *
 * Copyright 2015 Hyfn.com All Rights Reserved
 */

namespace App\Models\Api\V1;

/**
 * App\Models\Api\V1\Region
 *
 * @property integer $id 
 * @property string $name 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @property-read \Illuminate\Database\Eloquent\Collection|\Country[] $countries 
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Region whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Region whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Region whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Region whereUpdatedAt($value)
 */
class Region extends BaseApiModel {
    protected $guarded = array();

    public static $rules = array();

    public function countries() {
        return $this->hasMany('Country')->with('countryMarket');
    }
}