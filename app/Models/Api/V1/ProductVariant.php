<?php
/**
 * Project  : avon-scd-api
 *
 * User     : Chukky Nze <chukky.nze@hyfn.com>
 * Date     : 4/17/15
 * Time     : 11:56 AM
 * 
 * Copyright 2015 Hyfn.com All Rights Reserved
 */

namespace App\Models\Api\V1;

/**
 * App\Models\Api\V1\ProductVariant
 *
 * @property integer $id 
 * @property integer $market_id 
 * @property integer $product_id 
 * @property integer $feed_id 
 * @property integer $campaign_id 
 * @property boolean $can_buy 
 * @property string $fsc 
 * @property string $image 
 * @property string $line_number 
 * @property string $name 
 * @property string $sku 
 * @property boolean $is_active 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductVariant whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductVariant whereMarketId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductVariant whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductVariant whereFeedId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductVariant whereCampaignId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductVariant whereCanBuy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductVariant whereFsc($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductVariant whereImage($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductVariant whereLineNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductVariant whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductVariant whereSku($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductVariant whereIsActive($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductVariant whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductVariant whereUpdatedAt($value)
 */
class ProductVariant extends BaseApiModel
{
    protected $table        =   'scd_product_variants';
    protected $primaryKey   =   'id';
    protected $guarded      =   ['id'];
    protected $fillable     =   [
                                    'market_id',

                                    'product_id',
                                    'feed_id',
                                    'campaign_id',

                                    'can_buy',

                                    'fsc',
                                    'image',
                                    'line_number',
                                    'name',
                                    'sku',

                                    'is_active',
                                ];

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getMarketId()
    {
        return $this->market_id;
    }

    /**
     * @param int $market_id
     */
    public function setMarketId($market_id)
    {
        $this->market_id = $market_id;
    }

    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->product_id;
    }

    /**
     * @param int $product_id
     */
    public function setProductId($product_id)
    {
        $this->product_id = $product_id;
    }

    /**
     * @return int
     */
    public function getFeedId()
    {
        return $this->feed_id;
    }

    /**
     * @param int $feed_id
     */
    public function setFeedId($feed_id)
    {
        $this->feed_id = $feed_id;
    }

    /**
     * @return int
     */
    public function getCampaignId()
    {
        return $this->campaign_id;
    }

    /**
     * @param int $campaign_id
     */
    public function setCampaignId($campaign_id)
    {
        $this->campaign_id = $campaign_id;
    }

    /**
     * @return boolean
     */
    public function isCanBuy()
    {
        return $this->can_buy;
    }

    /**
     * @param boolean $can_buy
     */
    public function setCanBuy($can_buy)
    {
        $this->can_buy = $can_buy;
    }

    /**
     * @return string
     */
    public function getFsc()
    {
        return $this->fsc;
    }

    /**
     * @param string $fsc
     */
    public function setFsc($fsc)
    {
        $this->fsc = $fsc;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return string
     */
    public function getLineNumber()
    {
        return $this->line_number;
    }

    /**
     * @param string $line_number
     */
    public function setLineNumber($line_number)
    {
        $this->line_number = $line_number;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param string $sku
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    /**
     * @return boolean
     */
    public function isIsActive()
    {
        return $this->is_active;
    }

    /**
     * @param boolean $is_active
     */
    public function setIsActive($is_active)
    {
        $this->is_active = $is_active;
    }

    /**
     * @return \Carbon\Carbon
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param \Carbon\Carbon $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return \Carbon\Carbon
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param \Carbon\Carbon $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }




}