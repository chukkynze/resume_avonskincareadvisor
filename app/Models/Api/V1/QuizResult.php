<?php
/**
 * Project  : avon-scd-api
 *
 * User     : Chukky Nze <chukky.nze@hyfn.com>
 * Date     : 4/17/15
 * Time     : 11:58 AM
 *
 * Copyright 2015 Hyfn.com All Rights Reserved
 */

namespace App\Models\Api\V1;


/**
 * App\Models\Api\V1\QuizResult
 *
 * @property integer $id 
 * @property integer $market_id 
 * @property integer $quiz_user_id 
 * @property integer $quiz_id 
 * @property integer $question_id 
 * @property integer $answer_id 
 * @property integer $product_group_id 
 * @property integer $answer_product_id 
 * @property boolean $was_asked 
 * @property boolean $was_answered 
 * @property boolean $added_to_cart 
 * @property boolean $was_bought 
 * @property boolean $was_shared 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\QuizResult whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\QuizResult whereMarketId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\QuizResult whereQuizUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\QuizResult whereQuizId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\QuizResult whereQuestionId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\QuizResult whereAnswerId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\QuizResult whereProductGroupId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\QuizResult whereAnswerProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\QuizResult whereWasAsked($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\QuizResult whereWasAnswered($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\QuizResult whereAddedToCart($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\QuizResult whereWasBought($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\QuizResult whereWasShared($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\QuizResult whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\QuizResult whereUpdatedAt($value)
 */
class QuizResult extends BaseApiModel
{
    protected $table        =   'scd_quiz_results';
    protected $primaryKey   =   'id';
    protected $guarded      =   ['id'];
    protected $fillable     =   [
                                    'market_id',

                                    'quiz_user_id',
                                    'quiz_id',

                                    'question_id',
                                    'answer_id',
                                    'product_group_id',
                                    'answer_product_id',

                                    'was_asked',
                                    'was_answered',
                                    'added_to_cart',
                                    'was_bought',
                                    'was_shared',
                                ];
    protected $rules        =   [
                                    'market_id'         => 'required|numeric|exists:markets,market_id',
                                    'quiz_user_id'      => 'required|numeric|exists:scd_quiz_user,id',
                                    'quiz_id'           => 'required|numeric|exists:scd_quiz,id',
                                    'question_id'       => 'required|numeric|exists:scd_questions,id',
                                    'answer_id'         => 'required|numeric|exists:scd_answers,id',
                                    'product_group_id'  => 'required|numeric|exists:scd_product_groups,id',
                                    'answer_product_id' => 'required|numeric|exists:scd_answer_products,id',

                                    'was_asked'         => 'required|boolean',
                                    'was_answered'      => 'required|boolean',
                                    'added_to_cart'     => 'required|boolean',
                                    'was_bought'        => 'required|boolean',
                                    'was_shared'        => 'required|boolean',
                                ];

    public function getQuizResultByCustomerID($customerID)
    {

    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getMarketId()
    {
        return $this->market_id;
    }

    /**
     * @param int $market_id
     */
    public function setMarketId($market_id)
    {
        $this->market_id = $market_id;
    }

    /**
     * @return int
     */
    public function getQuizUserId()
    {
        return $this->quiz_user_id;
    }

    /**
     * @param int $quiz_user_id
     */
    public function setQuizUserId($quiz_user_id)
    {
        $this->quiz_user_id = $quiz_user_id;
    }

    /**
     * @return int
     */
    public function getQuizId()
    {
        return $this->quiz_id;
    }

    /**
     * @param int $quiz_id
     */
    public function setQuizId($quiz_id)
    {
        $this->quiz_id = $quiz_id;
    }

    /**
     * @return int
     */
    public function getQuestionId()
    {
        return $this->question_id;
    }

    /**
     * @param int $question_id
     */
    public function setQuestionId($question_id)
    {
        $this->question_id = $question_id;
    }

    /**
     * @return int
     */
    public function getAnswerId()
    {
        return $this->answer_id;
    }

    /**
     * @param int $answer_id
     */
    public function setAnswerId($answer_id)
    {
        $this->answer_id = $answer_id;
    }

    /**
     * @return int
     */
    public function getProductGroupId()
    {
        return $this->product_group_id;
    }

    /**
     * @param int $product_group_id
     */
    public function setProductGroupId($product_group_id)
    {
        $this->product_group_id = $product_group_id;
    }

    /**
     * @return int
     */
    public function getAnswerProductId()
    {
        return $this->answer_product_id;
    }

    /**
     * @param int $answer_product_id
     */
    public function setAnswerProductId($answer_product_id)
    {
        $this->answer_product_id = $answer_product_id;
    }

    /**
     * @return boolean
     */
    public function isWasAsked()
    {
        return $this->was_asked;
    }

    /**
     * @param boolean $was_asked
     */
    public function setWasAsked($was_asked)
    {
        $this->was_asked = $was_asked;
    }

    /**
     * @return boolean
     */
    public function isWasAnswered()
    {
        return $this->was_answered;
    }

    /**
     * @param boolean $was_answered
     */
    public function setWasAnswered($was_answered)
    {
        $this->was_answered = $was_answered;
    }

    /**
     * @return boolean
     */
    public function isAddedToCart()
    {
        return $this->added_to_cart;
    }

    /**
     * @param boolean $added_to_cart
     */
    public function setAddedToCart($added_to_cart)
    {
        $this->added_to_cart = $added_to_cart;
    }

    /**
     * @return boolean
     */
    public function isWasBought()
    {
        return $this->was_bought;
    }

    /**
     * @param boolean $was_bought
     */
    public function setWasBought($was_bought)
    {
        $this->was_bought = $was_bought;
    }

    /**
     * @return boolean
     */
    public function isWasShared()
    {
        return $this->was_shared;
    }

    /**
     * @param boolean $was_shared
     */
    public function setWasShared($was_shared)
    {
        $this->was_shared = $was_shared;
    }

    /**
     * @return \Carbon\Carbon
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param \Carbon\Carbon $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return \Carbon\Carbon
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param \Carbon\Carbon $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }


}