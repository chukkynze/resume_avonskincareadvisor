<?php
/**
 * Project  : avon-scd-api
 *
 * User     : Chukky Nze <chukky.nze@hyfn.com>
 * Date     : 4/17/15
 * Time     : 11:56 AM
 * 
 * Copyright 2015 Hyfn.com All Rights Reserved
 */

namespace App\Models\Api\V1;


/**
 * App\Models\Api\V1\AnswerProduct
 *
 * @property integer $id 
 * @property integer $market_id 
 * @property integer $product_group_id 
 * @property integer $product_id 
 * @property integer $replenish_product_id 
 * @property string $results_page_copy 
 * @property string $results_page_heading 
 * @property integer $results_page_step_id 
 * @property integer $results_page_product_usage_id 
 * @property integer $results_page_icon_usage_id 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @property string $results_page_order 
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\AnswerProduct whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\AnswerProduct whereMarketId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\AnswerProduct whereProductGroupId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\AnswerProduct whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\AnswerProduct whereReplenishProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\AnswerProduct whereResultsPageCopy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\AnswerProduct whereResultsPageHeading($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\AnswerProduct whereResultsPageStepId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\AnswerProduct whereResultsPageProductUsageId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\AnswerProduct whereResultsPageIconUsageId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\AnswerProduct whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\AnswerProduct whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\AnswerProduct whereResultsPageOrder($value)
 */
class AnswerProduct extends BaseApiModel
{
    protected $table        =   'scd_answer_products';
    protected $primaryKey   =   'id';
    protected $guarded      =   ['id'];
    protected $fillable     =   [
                                    'market_id',
                                    'product_group_id',
                                    'product_id',
                                    'replenish_product_id',

                                    'results_page_copy',
                                    'results_page_heading',

                                    'results_page_step_id',
                                    'results_page_product_usage_id',
                                    'results_page_icon_usage_id',

                                ];

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getMarketId()
    {
        return $this->market_id;
    }

    /**
     * @param int $market_id
     */
    public function setMarketId($market_id)
    {
        $this->market_id = $market_id;
    }

    /**
     * @return int
     */
    public function getProductGroupId()
    {
        return $this->product_group_id;
    }

    /**
     * @param int $product_group_id
     */
    public function setProductGroupId($product_group_id)
    {
        $this->product_group_id = $product_group_id;
    }

    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->product_id;
    }

    /**
     * @param int $product_id
     */
    public function setProductId($product_id)
    {
        $this->product_id = $product_id;
    }

    /**
     * @return int
     */
    public function getReplenishProductId()
    {
        return $this->replenish_product_id;
    }

    /**
     * @param int $replenish_product_id
     */
    public function setReplenishProductId($replenish_product_id)
    {
        $this->replenish_product_id = $replenish_product_id;
    }

    /**
     * @return string
     */
    public function getResultsPageCopy()
    {
        return $this->results_page_copy;
    }

    /**
     * @param string $results_page_copy
     */
    public function setResultsPageCopy($results_page_copy)
    {
        $this->results_page_copy = $results_page_copy;
    }

    /**
     * @return string
     */
    public function getResultsPageHeading()
    {
        return $this->results_page_heading;
    }

    /**
     * @param string $results_page_heading
     */
    public function setResultsPageHeading($results_page_heading)
    {
        $this->results_page_heading = $results_page_heading;
    }

    /**
     * @return int
     */
    public function getResultsPageStepId()
    {
        return $this->results_page_step_id;
    }

    /**
     * @param int $results_page_step_id
     */
    public function setResultsPageStepId($results_page_step_id)
    {
        $this->results_page_step_id = $results_page_step_id;
    }

    /**
     * @return int
     */
    public function getResultsPageProductUsageId()
    {
        return $this->results_page_product_usage_id;
    }

    /**
     * @param int $results_page_product_usage_id
     */
    public function setResultsPageProductUsageId($results_page_product_usage_id)
    {
        $this->results_page_product_usage_id = $results_page_product_usage_id;
    }

    /**
     * @return int
     */
    public function getResultsPageIconUsageId()
    {
        return $this->results_page_icon_usage_id;
    }

    /**
     * @param int $results_page_icon_usage_id
     */
    public function setResultsPageIconUsageId($results_page_icon_usage_id)
    {
        $this->results_page_icon_usage_id = $results_page_icon_usage_id;
    }

    /**
     * @return \Carbon\Carbon
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param \Carbon\Carbon $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return \Carbon\Carbon
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param \Carbon\Carbon $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    /**
     * @return string
     */
    public function getResultsPageOrder()
    {
        return $this->results_page_order;
    }

    /**
     * @param string $results_page_order
     */
    public function setResultsPageOrder($results_page_order)
    {
        $this->results_page_order = $results_page_order;
    }



}