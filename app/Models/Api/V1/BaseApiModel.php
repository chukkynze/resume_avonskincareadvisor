<?php namespace App\Models\Api\V1;

use App\Models\BaseModel;

/**
 * App\Models\Api\V1\BaseApiModel
 *
 */
class BaseApiModel extends BaseModel
{

    /**
     * User exposed observable events
     *
     * @var array
     */
    protected $observables = ['validating', 'validated'];


}