<?php
/**
 * Project  : avon-scd-api
 *
 * User     : Chukky Nze <chukky.nze@hyfn.com>
 * Date     : 4/17/15
 * Time     : 11:56 AM
 * 
 * Copyright 2015 Hyfn.com All Rights Reserved
 */

namespace App\Models\Api\V1;

/**
 * App\Models\Api\V1\Answer
 *
 * @property integer $id 
 * @property integer $question_id 
 * @property string $text 
 * @property string $thumbnail_url 
 * @property string $results_page_copy 
 * @property string $is_path_a 
 * @property string $is_path_b 
 * @property integer $order 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Answer whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Answer whereQuestionId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Answer whereText($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Answer whereThumbnailUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Answer whereResultsPageCopy($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Answer whereIsPathA($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Answer whereIsPathB($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Answer whereOrder($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Answer whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Answer whereUpdatedAt($value)
 */
class Answer extends BaseApiModel
{
    protected $table        =   'scd_answers';
    protected $primaryKey   =   'id';
    protected $guarded      =   ['id'];
    protected $fillable     =   [
                                    'question_id',

                                    'text',
                                    'thumbnail_url',

                                    'results_page_copy',
                                    'is_path_a',
                                    'is_path_b',

                                    'order',
                                ];

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getQuestionId()
    {
        return $this->question_id;
    }

    /**
     * @param int $question_id
     */
    public function setQuestionId($question_id)
    {
        $this->question_id = $question_id;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getThumbnailUrl()
    {
        return $this->thumbnail_url;
    }

    /**
     * @param string $thumbnail_url
     */
    public function setThumbnailUrl($thumbnail_url)
    {
        $this->thumbnail_url = $thumbnail_url;
    }

    /**
     * @return string
     */
    public function getResultsPageCopy()
    {
        return $this->results_page_copy;
    }

    /**
     * @param string $results_page_copy
     */
    public function setResultsPageCopy($results_page_copy)
    {
        $this->results_page_copy = $results_page_copy;
    }

    /**
     * @return string
     */
    public function getIsPathA()
    {
        return $this->is_path_a;
    }

    /**
     * @param string $is_path_a
     */
    public function setIsPathA($is_path_a)
    {
        $this->is_path_a = $is_path_a;
    }

    /**
     * @return string
     */
    public function getIsPathB()
    {
        return $this->is_path_b;
    }

    /**
     * @param string $is_path_b
     */
    public function setIsPathB($is_path_b)
    {
        $this->is_path_b = $is_path_b;
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param int $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

    /**
     * @return \Carbon\Carbon
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param \Carbon\Carbon $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return \Carbon\Carbon
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param \Carbon\Carbon $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }





}