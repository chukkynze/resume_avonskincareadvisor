<?php
/**
 * Project  : avon-scd-api
 *
 * User     : Chukky Nze <chukky.nze@hyfn.com>
 * Date     : 4/17/15
 * Time     : 11:56 AM
 *
 * Copyright 2015 Hyfn.com All Rights Reserved
 */

namespace App\Models\Api\V1;

/**
 * App\Models\Api\V1\Language
 *
 * @property integer $lang_id 
 * @property string $lang_name 
 * @property string $lang_abbr 
 * @property boolean $lang_active 
 * @property string $lang_image 
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Language whereLangId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Language whereLangName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Language whereLangAbbr($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Language whereLangActive($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Language whereLangImage($value)
 */
class Language extends BaseApiModel
{
    protected $table        =   'languages';
    protected $primaryKey   =   'lang_id';

    public static function getLanguageByLangCode($langCode)
    {
        return Language::where('lang_abbr',$langCode)->first();
    }

}