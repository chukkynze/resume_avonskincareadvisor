<?php
/**
 * Project  : avon-scd-api
 *
 * User     : Chukky Nze <chukky.nze@hyfn.com>
 * Date     : 4/17/15
 * Time     : 11:56 AM
 * 
 * Copyright 2015 Hyfn.com All Rights Reserved
 */

namespace App\Models\Api\V1;


/**
 * App\Models\Api\V1\Representative
 *
 * @property integer $market_id 
 * @property string $rep_privatekey 
 * @property string $rep_pubkey 
 * @property string $rep_username 
 * @property boolean $rep_isTest 
 * @property string $rep_photo 
 * @property string $rep_fname 
 * @property string $rep_lname 
 * @property string $rep_email 
 * @property string $rep_phone 
 * @property string $rep_about 
 * @property string $rep_pos 
 * @property boolean $rep_optOut 
 * @property string $mktcd 
 * @property string $aux 
 * @property string $mailplan 
 * @property string $rep_datetime_updated 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @property boolean $find_rep_opt_in 
 * @property string $additional_data 
 * @property integer $rep_vanity_id 
 * @property integer $address_id 
 * @property boolean $show_on_map 
 * @property string $cust_rtg 
 * @property string $mrktg_msg 
 * @property string $mrktg_url 
 * @property boolean $allow_text_message 
 * @property string $rep_photo_unbadged 
 * @property-read \App\Models\Api\V1\Product $product 
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Representative whereMarketId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Representative whereRepPrivatekey($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Representative whereRepPubkey($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Representative whereRepUsername($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Representative whereRepIsTest($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Representative whereRepPhoto($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Representative whereRepFname($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Representative whereRepLname($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Representative whereRepEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Representative whereRepPhone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Representative whereRepAbout($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Representative whereRepPos($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Representative whereRepOptOut($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Representative whereMktcd($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Representative whereAux($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Representative whereMailplan($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Representative whereRepDatetimeUpdated($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Representative whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Representative whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Representative whereFindRepOptIn($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Representative whereAdditionalData($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Representative whereRepVanityId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Representative whereAddressId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Representative whereShowOnMap($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Representative whereCustRtg($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Representative whereMrktgMsg($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Representative whereMrktgUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Representative whereAllowTextMessage($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Representative whereRepPhotoUnbadged($value)
 */
class Representative extends BaseApiModel
{
    protected $dsiCampaignLookupURL     =   '';
    protected $dsiCampaignLookupHeader  =   '';
    protected $userAgent                =   "";


    protected $table        =   'rep_profile';
    protected $primaryKey   =   'rep_privatekey';
    public $timestamps      =   false;


    /**
     * Check that the pubkey passed exists.
     *
     * @author Chukky Nze <chukky.nze@hyfn.com>
     *
     * @param $publicKey
     * @return bool
     */
    public static function validateRepByPublicKey($publicKey)
    {
        $count  = self::whereRepPubkey($publicKey)->count();
        return ($count == 1 ? true : false);
    }


    /**
     * Check that the rep id, which is actually the rep username, exists.
     *
     * @author Chukky Nze <chukky.nze@hyfn.com>
     *
     * @param $repId
     * @return bool
     */
    public static function validateRepById($repId)
    {
        $count  = self::whereRepUsername($repId)->count();
        return ($count >= 1 ? true : false);
    }


    /**
     * Use the rep pubkey to get rep data
     *
     * @author Chukky Nze <chukky.nze@hyfn.com>
     *
     * @param $publicKey
     * @param $marketID
     * @return array
     */
    public static function getRepDataWithRepPubkey($publicKey, $marketID)
    {
        if(isset($publicKey))
        {
            if(isset($marketID))
            {
                $output =   self::getRepData
                            (
                                self::whereRepPubkey($publicKey)
                                ->first
                                ([
                                    'market_id',
                                    'rep_pubkey',
                                    'rep_username',
                                    'rep_isTest',
                                    'rep_photo',
                                    'rep_fname',
                                    'rep_lname',
                                    'rep_email',
                                    'rep_phone',
                                    'rep_about',
                                    'rep_pos',
                                    'rep_optOut',
                                    'mktcd',
                                    'aux',
                                    'mailplan',
                                    'rep_datetime_updated',
                                    'created_at',
                                    'updated_at',
                                    'find_rep_opt_in',
                                    'additional_data',
                                    'rep_vanity_id',
                                    'address_id',
                                    'show_on_map',
                                    'cust_rtg',
                                    'mrktg_msg',
                                    'mrktg_url',
                                    'allow_text_message',
                                    'rep_photo_unbadged',
                                ]),
                                $marketID
                            );
            }
            else
            {
                $output =   ['Invalid Market ID.'];
            }
        }
        else
        {
            $output =   ['Invalid Rep PubKey.'];
        }

        return $output;
    }


    /**
     * Use the rep id/username to get rep data
     *
     * @author Chukky Nze <chukky.nze@hyfn.com>
     *
     * @param $repUsername
     * @param $marketID
     * @return array
     */
    public static function getRepDataWithRepUsername($repUsername, $marketID)
    {
        if(isset($repUsername))
        {
            if(isset($marketID))
            {
                $output =   self::getRepData
                            (
                                self::whereRepUsername($repUsername)
                                    ->first
                                    ([
                                        'market_id',
                                        'rep_pubkey',
                                        'rep_username',
                                        'rep_isTest',
                                        'rep_photo',
                                        'rep_fname',
                                        'rep_lname',
                                        'rep_email',
                                        'rep_phone',
                                        'rep_about',
                                        'rep_pos',
                                        'rep_optOut',
                                        'mktcd',
                                        'aux',
                                        'mailplan',
                                        'rep_datetime_updated',
                                        'created_at',
                                        'updated_at',
                                        'find_rep_opt_in',
                                        'additional_data',
                                        'rep_vanity_id',
                                        'address_id',
                                        'show_on_map',
                                        'cust_rtg',
                                        'mrktg_msg',
                                        'mrktg_url',
                                        'allow_text_message',
                                        'rep_photo_unbadged',
                                    ]),
                                $marketID
                            );
            }
            else
            {
                $output =   ['Invalid Market ID.'];
            }
        }
        else
        {
            $output =   ['Invalid Rep Username.'];
        }

        return $output;
    }


    /**
     * Given a valid Representative object, get the rep details
     *
     * @author Chukky Nze <chukky.nze@hyfn.com>
     *
     * @param Representative $repObject
     * @param $marketID
     *
     * @return array
     */
    public static function getRepData(Representative $repObject, $marketID)
    {
        if(is_object($repObject))
        {
            $DSICampaignData    =   self::getDSICampaignData($repObject->getRepUsername());
            $repCampaignData    =   self::getRepCampaignData($repObject->getRepUsername(), $marketID);
            $repData            =   $repObject->toArray();

            $output             =   [
                'repData'           =>  $repData,
                'DSICampaignData'   =>  $DSICampaignData,
                'repCampaignData'   =>  $repCampaignData,
            ];
        }
        else
        {
            $output = ['Invalid Representative resource passed.'];
        }

        return $output;
    }


    public static function getDSICampaignData($repUsername)
    {
        $dsiLookupURL               =   'https://shopapi.avon.com/2/representative/campaign/' . $repUsername;
        $userAgent                  =   'Googlebot/2.1 ( http://www.googlebot.com/bot.html)';
        $dsiCampaignLookupHeader    =   [
                                            'brand: Avon',
                                            //'apiKey: 07DACE10-F343-45A0-ACB8-1AF349B40387', // old qaf api key
                                            'apiKey: 5AD50F00-3E7C-4CEF-BF3B-4B1FAC0861E8', // new shopapi api ky
                                            'language: English',
                                        ];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_HTTPHEADER     ,   $dsiCampaignLookupHeader);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER ,   FALSE);
        curl_setopt($ch, CURLOPT_URL            ,   $dsiLookupURL);
        curl_setopt($ch, CURLOPT_USERAGENT      ,   $userAgent);
        curl_setopt($ch, CURLOPT_FAILONERROR    ,   1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER ,   1);
        curl_setopt($ch, CURLOPT_TIMEOUT        ,   3); // times out after 4s

        $response = curl_exec($ch);
        curl_close($ch);
        $result = null;

        if (strlen(trim($response)))
        {
            $json = json_decode($response, TRUE);
            if (array_key_exists('Res', $json) && $json['Res'] != "Invalid")
            {
                $campaign = $json['Res'];
                if (is_numeric($campaign))
                {
                    $c1 = [substr($campaign, 4,2), substr($campaign, 0,4)];
                }
                $result = [$c1];
            }
        }
        return $result;
    }


    public static function getRepCampaignData($repUsername, $marketID)
    {
        $Market             =   Market::whereMarketId($marketID);
        $marketCode         =   (isset($Market->market_mktcd) ?: 0);
        $campaignLookupURL  =   'http://avondigital.com/WebServices/GetRepInfo.php';
        $postVariables      =   'requester=hyfn&market='. $marketCode .'&repid=' . $repUsername;

        $ch = curl_init($campaignLookupURL);

        curl_setopt( $ch, CURLOPT_POST          , 1);
        curl_setopt( $ch, CURLOPT_POSTFIELDS    , $postVariables);
        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt( $ch, CURLOPT_HEADER        , 0);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);

        $response   =   curl_exec( $ch );
        curl_close($ch);
        $xml        =   simplexml_load_string($response);
        $result     =   null;

        if (
                    $xml->errorcode == 0
                &&  $xml->campaigns
                &&  $xml->campaigns->campaign
        )
        {
            $campaign   =   $xml->campaigns->campaign;
            $c1         =   [
                                (string)$campaign['number'],
                                (string)$campaign['year']
                            ];
            $result     =   [$c1];
        }
        else
        {
            $result = null;
        }

        return $result;
    }


    public function product()
    {
        return $this->hasOne('App\Models\Api\V1\Product');
    }

    /**
     * @return int
     */
    public function getMarketId()
    {
        return $this->market_id;
    }

    /**
     * @param int $market_id
     */
    public function setMarketId($market_id)
    {
        $this->market_id = $market_id;
    }

    /**
     * @return string
     */
    public function getRepPrivatekey()
    {
        return $this->rep_privatekey;
    }

    /**
     * @param string $rep_privatekey
     */
    public function setRepPrivatekey($rep_privatekey)
    {
        $this->rep_privatekey = $rep_privatekey;
    }

    /**
     * @return string
     */
    public function getRepPubkey()
    {
        return $this->rep_pubkey;
    }

    /**
     * @param string $rep_pubkey
     */
    public function setRepPubkey($rep_pubkey)
    {
        $this->rep_pubkey = $rep_pubkey;
    }

    /**
     * @return string
     */
    public function getRepUsername()
    {
        return $this->rep_username;
    }

    /**
     * @param string $rep_username
     */
    public function setRepUsername($rep_username)
    {
        $this->rep_username = $rep_username;
    }

    /**
     * @return boolean
     */
    public function isRepIsTest()
    {
        return $this->rep_isTest;
    }

    /**
     * @param boolean $rep_isTest
     */
    public function setRepIsTest($rep_isTest)
    {
        $this->rep_isTest = $rep_isTest;
    }

    /**
     * @return string
     */
    public function getRepPhoto()
    {
        return $this->rep_photo;
    }

    /**
     * @param string $rep_photo
     */
    public function setRepPhoto($rep_photo)
    {
        $this->rep_photo = $rep_photo;
    }

    /**
     * @return string
     */
    public function getRepFname()
    {
        return $this->rep_fname;
    }

    /**
     * @param string $rep_fname
     */
    public function setRepFname($rep_fname)
    {
        $this->rep_fname = $rep_fname;
    }

    /**
     * @return string
     */
    public function getRepLname()
    {
        return $this->rep_lname;
    }

    /**
     * @param string $rep_lname
     */
    public function setRepLname($rep_lname)
    {
        $this->rep_lname = $rep_lname;
    }

    /**
     * @return string
     */
    public function getRepEmail()
    {
        return $this->rep_email;
    }

    /**
     * @param string $rep_email
     */
    public function setRepEmail($rep_email)
    {
        $this->rep_email = $rep_email;
    }

    /**
     * @return string
     */
    public function getRepPhone()
    {
        return $this->rep_phone;
    }

    /**
     * @param string $rep_phone
     */
    public function setRepPhone($rep_phone)
    {
        $this->rep_phone = $rep_phone;
    }

    /**
     * @return string
     */
    public function getRepAbout()
    {
        return $this->rep_about;
    }

    /**
     * @param string $rep_about
     */
    public function setRepAbout($rep_about)
    {
        $this->rep_about = $rep_about;
    }

    /**
     * @return string
     */
    public function getRepPos()
    {
        return $this->rep_pos;
    }

    /**
     * @param string $rep_pos
     */
    public function setRepPos($rep_pos)
    {
        $this->rep_pos = $rep_pos;
    }

    /**
     * @return boolean
     */
    public function isRepOptOut()
    {
        return $this->rep_optOut;
    }

    /**
     * @param boolean $rep_optOut
     */
    public function setRepOptOut($rep_optOut)
    {
        $this->rep_optOut = $rep_optOut;
    }

    /**
     * @return string
     */
    public function getMktcd()
    {
        return $this->mktcd;
    }

    /**
     * @param string $mktcd
     */
    public function setMktcd($mktcd)
    {
        $this->mktcd = $mktcd;
    }

    /**
     * @return string
     */
    public function getAux()
    {
        return $this->aux;
    }

    /**
     * @param string $aux
     */
    public function setAux($aux)
    {
        $this->aux = $aux;
    }

    /**
     * @return string
     */
    public function getMailplan()
    {
        return $this->mailplan;
    }

    /**
     * @param string $mailplan
     */
    public function setMailplan($mailplan)
    {
        $this->mailplan = $mailplan;
    }

    /**
     * @return string
     */
    public function getRepDatetimeUpdated()
    {
        return $this->rep_datetime_updated;
    }

    /**
     * @param string $rep_datetime_updated
     */
    public function setRepDatetimeUpdated($rep_datetime_updated)
    {
        $this->rep_datetime_updated = $rep_datetime_updated;
    }

    /**
     * @return \Carbon\Carbon
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param \Carbon\Carbon $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return \Carbon\Carbon
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param \Carbon\Carbon $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    /**
     * @return boolean
     */
    public function isFindRepOptIn()
    {
        return $this->find_rep_opt_in;
    }

    /**
     * @param boolean $find_rep_opt_in
     */
    public function setFindRepOptIn($find_rep_opt_in)
    {
        $this->find_rep_opt_in = $find_rep_opt_in;
    }

    /**
     * @return string
     */
    public function getAdditionalData()
    {
        return $this->additional_data;
    }

    /**
     * @param string $additional_data
     */
    public function setAdditionalData($additional_data)
    {
        $this->additional_data = $additional_data;
    }

    /**
     * @return int
     */
    public function getRepVanityId()
    {
        return $this->rep_vanity_id;
    }

    /**
     * @param int $rep_vanity_id
     */
    public function setRepVanityId($rep_vanity_id)
    {
        $this->rep_vanity_id = $rep_vanity_id;
    }

    /**
     * @return int
     */
    public function getAddressId()
    {
        return $this->address_id;
    }

    /**
     * @param int $address_id
     */
    public function setAddressId($address_id)
    {
        $this->address_id = $address_id;
    }

    /**
     * @return boolean
     */
    public function isShowOnMap()
    {
        return $this->show_on_map;
    }

    /**
     * @param boolean $show_on_map
     */
    public function setShowOnMap($show_on_map)
    {
        $this->show_on_map = $show_on_map;
    }

    /**
     * @return string
     */
    public function getCustRtg()
    {
        return $this->cust_rtg;
    }

    /**
     * @param string $cust_rtg
     */
    public function setCustRtg($cust_rtg)
    {
        $this->cust_rtg = $cust_rtg;
    }

    /**
     * @return string
     */
    public function getMrktgMsg()
    {
        return $this->mrktg_msg;
    }

    /**
     * @param string $mrktg_msg
     */
    public function setMrktgMsg($mrktg_msg)
    {
        $this->mrktg_msg = $mrktg_msg;
    }

    /**
     * @return string
     */
    public function getMrktgUrl()
    {
        return $this->mrktg_url;
    }

    /**
     * @param string $mrktg_url
     */
    public function setMrktgUrl($mrktg_url)
    {
        $this->mrktg_url = $mrktg_url;
    }

    /**
     * @return boolean
     */
    public function isAllowTextMessage()
    {
        return $this->allow_text_message;
    }

    /**
     * @param boolean $allow_text_message
     */
    public function setAllowTextMessage($allow_text_message)
    {
        $this->allow_text_message = $allow_text_message;
    }

    /**
     * @return string
     */
    public function getRepPhotoUnbadged()
    {
        return $this->rep_photo_unbadged;
    }

    /**
     * @param string $rep_photo_unbadged
     */
    public function setRepPhotoUnbadged($rep_photo_unbadged)
    {
        $this->rep_photo_unbadged = $rep_photo_unbadged;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param Product $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }


}