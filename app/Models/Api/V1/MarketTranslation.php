<?php
/**
 * Project  : avon-scd-api
 *
 * User     : Chukky Nze <chukky.nze@hyfn.com>
 * Date     : 4/17/15
 * Time     : 11:56 AM
 *
 * Copyright 2015 Hyfn.com All Rights Reserved
 */

namespace App\Models\Api\V1;

/**
 * App\Models\Api\V1\MarketTranslation
 *
 * @property integer $ml_id 
 * @property integer $market_id 
 * @property string $market_section 
 * @property string $ml_name 
 * @property string $ml_sub 
 * @property string $ml_type 
 * @property string $ml_key 
 * @property string $ml_val 
 * @property string $ml_ref 
 * @property string $ml_default 
 * @property boolean $ml_changed 
 * @property boolean $ml_market_avail 
 * @property string $ml_section_category 
 * @property integer $ml_index 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\MarketTranslation whereMlId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\MarketTranslation whereMarketId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\MarketTranslation whereMarketSection($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\MarketTranslation whereMlName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\MarketTranslation whereMlSub($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\MarketTranslation whereMlType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\MarketTranslation whereMlKey($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\MarketTranslation whereMlVal($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\MarketTranslation whereMlRef($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\MarketTranslation whereMlDefault($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\MarketTranslation whereMlChanged($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\MarketTranslation whereMlMarketAvail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\MarketTranslation whereMlSectionCategory($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\MarketTranslation whereMlIndex($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\MarketTranslation whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\MarketTranslation whereUpdatedAt($value)
 */
class MarketTranslation extends BaseApiModel
{
    protected $table        =   'markets_lang';
    protected $primaryKey   =   'ml_id';

    public $timestamps      =   false;

    public static function getTranslationsByMarketId($marketID)
    {
        $output         =   [];
        $translations   =   self::whereMarketId($marketID)->
                            whereMlMarketAvail(1)->
                            whereMarketSection('scd_tool')->
                            get
                            ([
                                'ml_type',
                                'ml_key',
                                'ml_val',
                                'ml_default',
                                'ml_section_category',
                            ]);

        foreach ($translations as $translation)
        {
            $output[$translation->ml_key]   =   $translation->ml_val;
        }

        return $output;

    }
}