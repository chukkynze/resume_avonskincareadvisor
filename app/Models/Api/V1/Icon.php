<?php
/**
 * Project  : avon-scd-api
 *
 * User     : Chukky Nze <chukky.nze@hyfn.com>
 * Date     : 4/17/15
 * Time     : 11:56 AM
 * 
 * Copyright 2015 Hyfn.com All Rights Reserved
 */

namespace App\Models\Api\V1;


/**
 * App\Models\Api\V1\Icon
 *
 * @property integer $id 
 * @property integer $market_id 
 * @property string $image_url 
 * @property string $description 
 * @property string $type 
 * @property integer $order 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @property string $results_page_order_option 
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Icon whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Icon whereMarketId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Icon whereImageUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Icon whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Icon whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Icon whereOrder($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Icon whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Icon whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Icon whereResultsPageOrderOption($value)
 */
class Icon extends BaseApiModel
{

    protected $table        =   'scd_icons';
    protected $primaryKey   =   'id';
    protected $guarded      =   ['id'];
    protected $fillable     =   [
                                    'market_id',

                                    'image_url',
                                    'description',
                                    'type',

                                    'order',
                                ];

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getMarketId()
    {
        return $this->market_id;
    }

    /**
     * @param int $market_id
     */
    public function setMarketId($market_id)
    {
        $this->market_id = $market_id;
    }

    /**
     * @return string
     */
    public function getImageUrl()
    {
        return $this->image_url;
    }

    /**
     * @param string $image_url
     */
    public function setImageUrl($image_url)
    {
        $this->image_url = $image_url;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param int $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

    /**
     * @return \Carbon\Carbon
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param \Carbon\Carbon $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return \Carbon\Carbon
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param \Carbon\Carbon $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    /**
     * @return string
     */
    public function getResultsPageOrderOption()
    {
        return $this->results_page_order_option;
    }

    /**
     * @param string $results_page_order_option
     */
    public function setResultsPageOrderOption($results_page_order_option)
    {
        $this->results_page_order_option = $results_page_order_option;
    }


}