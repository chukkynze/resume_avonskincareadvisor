<?php
/**
 * Project  : avon-scd-api
 *
 * User     : Chukky Nze <chukky.nze@hyfn.com>
 * Date     : 4/17/15
 * Time     : 11:56 AM
 * 
 * Copyright 2015 Hyfn.com All Rights Reserved
 */

namespace App\Models\Api\V1;

/**
 * App\Models\Api\V1\ProductCampaignDetails
 *
 * @property integer $id 
 * @property integer $market_id 
 * @property integer $product_id 
 * @property integer $feed_id 
 * @property integer $campaign_id 
 * @property float $list_price 
 * @property float $sales_price 
 * @property float $auto_replenishment_price 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductCampaignDetails whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductCampaignDetails whereMarketId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductCampaignDetails whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductCampaignDetails whereFeedId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductCampaignDetails whereCampaignId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductCampaignDetails whereListPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductCampaignDetails whereSalesPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductCampaignDetails whereAutoReplenishmentPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductCampaignDetails whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductCampaignDetails whereUpdatedAt($value)
 */
class ProductCampaignDetails extends BaseApiModel
{
    protected $table        =   'scd_product_campaign_details';
    protected $primaryKey   =   'id';
    protected $guarded      =   ['id'];
    protected $fillable     =   [
                                    'market_id',

                                    'product_id',
                                    'feed_id',
                                    'campaign_id',

                                    'list_price',
                                    'sales_price',
                                    'auto_replenishment_price',

                                ];

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getMarketId()
    {
        return $this->market_id;
    }

    /**
     * @param int $market_id
     */
    public function setMarketId($market_id)
    {
        $this->market_id = $market_id;
    }

    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->product_id;
    }

    /**
     * @param int $product_id
     */
    public function setProductId($product_id)
    {
        $this->product_id = $product_id;
    }

    /**
     * @return int
     */
    public function getFeedId()
    {
        return $this->feed_id;
    }

    /**
     * @param int $feed_id
     */
    public function setFeedId($feed_id)
    {
        $this->feed_id = $feed_id;
    }

    /**
     * @return int
     */
    public function getCampaignId()
    {
        return $this->campaign_id;
    }

    /**
     * @param int $campaign_id
     */
    public function setCampaignId($campaign_id)
    {
        $this->campaign_id = $campaign_id;
    }

    /**
     * @return float
     */
    public function getListPrice()
    {
        return $this->list_price;
    }

    /**
     * @param float $list_price
     */
    public function setListPrice($list_price)
    {
        $this->list_price = $list_price;
    }

    /**
     * @return float
     */
    public function getSalesPrice()
    {
        return $this->sales_price;
    }

    /**
     * @param float $sales_price
     */
    public function setSalesPrice($sales_price)
    {
        $this->sales_price = $sales_price;
    }

    /**
     * @return float
     */
    public function getAutoReplenishmentPrice()
    {
        return $this->auto_replenishment_price;
    }

    /**
     * @param float $auto_replenishment_price
     */
    public function setAutoReplenishmentPrice($auto_replenishment_price)
    {
        $this->auto_replenishment_price = $auto_replenishment_price;
    }

    /**
     * @return \Carbon\Carbon
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param \Carbon\Carbon $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return \Carbon\Carbon
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param \Carbon\Carbon $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }



}