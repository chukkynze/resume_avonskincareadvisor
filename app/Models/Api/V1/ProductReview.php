<?php
/**
 * Project  : avon-scd-api
 *
 * User     : Chukky Nze <chukky.nze@hyfn.com>
 * Date     : 4/17/15
 * Time     : 11:56 AM
 * 
 * Copyright 2015 Hyfn.com All Rights Reserved
 */

namespace App\Models\Api\V1;

/**
 * App\Models\Api\V1\ProductReview
 *
 * @property integer $id 
 * @property integer $market_id 
 * @property integer $product_id 
 * @property integer $feed_id 
 * @property integer $campaign_id 
 * @property integer $average_rating 
 * @property integer $total_reviews 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductReview whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductReview whereMarketId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductReview whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductReview whereFeedId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductReview whereCampaignId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductReview whereAverageRating($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductReview whereTotalReviews($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductReview whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductReview whereUpdatedAt($value)
 */
class ProductReview extends BaseApiModel
{
    protected $table        =   'scd_product_reviews';
    protected $primaryKey   =   'id';
    protected $guarded      =   ['id'];
    protected $fillable     =   [
                                    'market_id',

                                    'product_id',
                                    'feed_id',
                                    'campaign_id',

                                    'average_rating',
                                    'total_reviews',
                                ];

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getMarketId()
    {
        return $this->market_id;
    }

    /**
     * @param int $market_id
     */
    public function setMarketId($market_id)
    {
        $this->market_id = $market_id;
    }

    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->product_id;
    }

    /**
     * @param int $product_id
     */
    public function setProductId($product_id)
    {
        $this->product_id = $product_id;
    }

    /**
     * @return int
     */
    public function getFeedId()
    {
        return $this->feed_id;
    }

    /**
     * @param int $feed_id
     */
    public function setFeedId($feed_id)
    {
        $this->feed_id = $feed_id;
    }

    /**
     * @return int
     */
    public function getCampaignId()
    {
        return $this->campaign_id;
    }

    /**
     * @param int $campaign_id
     */
    public function setCampaignId($campaign_id)
    {
        $this->campaign_id = $campaign_id;
    }

    /**
     * @return int
     */
    public function getAverageRating()
    {
        return $this->average_rating;
    }

    /**
     * @param int $average_rating
     */
    public function setAverageRating($average_rating)
    {
        $this->average_rating = $average_rating;
    }

    /**
     * @return int
     */
    public function getTotalReviews()
    {
        return $this->total_reviews;
    }

    /**
     * @param int $total_reviews
     */
    public function setTotalReviews($total_reviews)
    {
        $this->total_reviews = $total_reviews;
    }

    /**
     * @return \Carbon\Carbon
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param \Carbon\Carbon $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return \Carbon\Carbon
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param \Carbon\Carbon $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }



}