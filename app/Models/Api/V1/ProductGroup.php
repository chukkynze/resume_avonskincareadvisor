<?php
/**
 * Project  : avon-scd-api
 *
 * A "Product Group" is also known as a "Regimen", but product group flows better when reading the logic.
 *
 * User     : Chukky Nze <chukky.nze@hyfn.com>
 * Date     : 4/17/15
 * Time     : 11:56 AM
 * 
 * Copyright 2015 Hyfn.com All Rights Reserved
 */

namespace App\Models\Api\V1;

/**
 * App\Models\Api\V1\ProductGroup
 *
 * @property integer $id 
 * @property integer $market_id 
 * @property integer $answer_id 
 * @property string $regimen_description 
 * @property string $regimen_image_url 
 * @property string $regimen_secondary_image 
 * @property integer $replenish_product_id 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @property integer $regimen_product_id 
 * @property integer $regimen_step_id 
 * @property integer $regimen_usage_id 
 * @property integer $regimen_results_page_icon_usage_id 
 * @property string $regimen_results_page_order 
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductGroup whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductGroup whereMarketId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductGroup whereAnswerId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductGroup whereRegimenDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductGroup whereRegimenImageUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductGroup whereRegimenSecondaryImage($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductGroup whereReplenishProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductGroup whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductGroup whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductGroup whereRegimenProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductGroup whereRegimenStepId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductGroup whereRegimenUsageId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductGroup whereRegimenResultsPageIconUsageId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductGroup whereRegimenResultsPageOrder($value)
 */
class ProductGroup extends BaseApiModel
{
    protected $table        =   'scd_product_groups';
    protected $primaryKey   =   'id';
    protected $guarded      =   ['id'];
    protected $fillable     =   [
                                    'market_id',
                                    'answer_id',

                                    'regimen_description',
                                    'regimen_image_url',
                                    'regimen_secondary_image',

                                    'replenish_product_id',
                                ];

    /**
     * @return int
     */
    public function getRegimenProductId()
    {
        return $this->regimen_product_id;
    }

    /**
     * @param int $regimen_product_id
     */
    public function setRegimenProductId($regimen_product_id)
    {
        $this->regimen_product_id = $regimen_product_id;
    }

    /**
     * @return int
     */
    public function getRegimenStepId()
    {
        return $this->regimen_step_id;
    }

    /**
     * @param int $regimen_step_id
     */
    public function setRegimenStepId($regimen_step_id)
    {
        $this->regimen_step_id = $regimen_step_id;
    }

    /**
     * @return int
     */
    public function getRegimenUsageId()
    {
        return $this->regimen_usage_id;
    }

    /**
     * @param int $regimen_usage_id
     */
    public function setRegimenUsageId($regimen_usage_id)
    {
        $this->regimen_usage_id = $regimen_usage_id;
    }

    /**
     * @return int
     */
    public function getRegimenResultsPageIconUsageId()
    {
        return $this->regimen_results_page_icon_usage_id;
    }

    /**
     * @param int $regimen_results_page_icon_usage_id
     */
    public function setRegimenResultsPageIconUsageId($regimen_results_page_icon_usage_id)
    {
        $this->regimen_results_page_icon_usage_id = $regimen_results_page_icon_usage_id;
    }

    /**
     * @return string
     */
    public function getRegimenResultsPageOrder()
    {
        return $this->regimen_results_page_order;
    }

    /**
     * @param string $regimen_results_page_order
     */
    public function setRegimenResultsPageOrder($regimen_results_page_order)
    {
        $this->regimen_results_page_order = $regimen_results_page_order;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getMarketId()
    {
        return $this->market_id;
    }

    /**
     * @param int $market_id
     */
    public function setMarketId($market_id)
    {
        $this->market_id = $market_id;
    }

    /**
     * @return int
     */
    public function getAnswerId()
    {
        return $this->answer_id;
    }

    /**
     * @param int $answer_id
     */
    public function setAnswerId($answer_id)
    {
        $this->answer_id = $answer_id;
    }

    /**
     * @return string
     */
    public function getRegimenDescription()
    {
        return $this->regimen_description;
    }

    /**
     * @param string $regimen_description
     */
    public function setRegimenDescription($regimen_description)
    {
        $this->regimen_description = $regimen_description;
    }

    /**
     * @return string
     */
    public function getRegimenImageUrl()
    {
        return $this->regimen_image_url;
    }

    /**
     * @param string $regimen_image_url
     */
    public function setRegimenImageUrl($regimen_image_url)
    {
        $this->regimen_image_url = $regimen_image_url;
    }

    /**
     * @return string
     */
    public function getRegimenSecondaryImage()
    {
        return $this->regimen_secondary_image;
    }

    /**
     * @param string $regimen_secondary_image
     */
    public function setRegimenSecondaryImage($regimen_secondary_image)
    {
        $this->regimen_secondary_image = $regimen_secondary_image;
    }

    /**
     * @return int
     */
    public function getReplenishProductId()
    {
        return $this->replenish_product_id;
    }

    /**
     * @param int $replenish_product_id
     */
    public function setReplenishProductId($replenish_product_id)
    {
        $this->replenish_product_id = $replenish_product_id;
    }

    /**
     * @return \Carbon\Carbon
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param \Carbon\Carbon $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return \Carbon\Carbon
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param \Carbon\Carbon $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }



}