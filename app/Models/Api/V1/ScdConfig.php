<?php
/**
 * Project  : avon-scd-api
 *
 * User     : Chukky Nze <chukky.nze@hyfn.com>
 * Date     : 4/17/15
 * Time     : 11:56 AM
 * 
 * Copyright 2015 Hyfn.com All Rights Reserved
 */

namespace App\Models\Api\V1;

use DB;
use Cache;
use Schema;

/**
 * App\Models\Api\V1\ScdConfig
 *
 * @property integer $id 
 * @property integer $market_id 
 * @property string $products_api_feed_url 
 * @property string $dsi_dept_search_term 
 * @property boolean $use_dsi_feed 
 * @property boolean $edit_dsi_feed_products 
 * @property integer $num_addtodb_products_selectable 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @property boolean $facebook_login 
 * @property boolean $site_login 
 * @property boolean $registration 
 * @property boolean $forgot_password 
 * @property string $registration_url 
 * @property string $forgot_password_url 
 * @property string $rep_campaign_source 
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ScdConfig whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ScdConfig whereMarketId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ScdConfig whereProductsApiFeedUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ScdConfig whereDsiDeptSearchTerm($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ScdConfig whereUseDsiFeed($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ScdConfig whereEditDsiFeedProducts($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ScdConfig whereNumAddtodbProductsSelectable($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ScdConfig whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ScdConfig whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ScdConfig whereFacebookLogin($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ScdConfig whereSiteLogin($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ScdConfig whereRegistration($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ScdConfig whereForgotPassword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ScdConfig whereRegistrationUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ScdConfig whereForgotPasswordUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ScdConfig whereRepCampaignSource($value)
 */
class ScdConfig extends BaseApiModel
{
    protected $table        =   'markets_config_scdtool';
    protected $primaryKey   =   'id';
    public $timestamps      =   false;


    public static function getConfigData($marketID)
    {
        if(Schema::hasTable('markets_config_scdtool'))
        {
            $output             =   [];
            $rawConfigObject    =   self::whereMarketId($marketID)->first();
            if(!is_null($rawConfigObject))
            {
                $rawConfigData      =   $rawConfigObject->toArray();
                foreach ($rawConfigData as $key => $data)
                {
                    $output[$key]   =  $data;
                }

                $genericCampaignData  =   self::getGenericCampaignData($marketID);

                foreach ($genericCampaignData as $key => $data)
                {
                    $output[$key]   =  $data;
                }

                if(isset($rawConfigData['rep_campaign_source']))
                {
                    switch($rawConfigData['rep_campaign_source'])
                    {
                        case 'dsi'              :   $campaignID         =   NULL;
                            $campaignIDSource   =   '';
                            break;
                        case 'get_rep_info'     :   $campaignID         =   NULL;
                            $campaignIDSource   =   '';
                            break;

                        default :
                            $campaignID         =   $genericCampaignData->generic_campaign_id;
                            $campaignIDSource   =   'market_generic';
                    }

                    if(isset($campaignID))
                    {
                        $output['activeCampaignID']         =   $campaignID;
                        $output['activeCampaignIDSource']   =   $campaignIDSource;
                    }
                    else
                    {
                        $output['activeCampaignID']         =   $genericCampaignData->generic_campaign_id;
                        $output['activeCampaignIDSource']   =   'market_generic';
                    }
                }
                else
                {
                    $output['activeCampaignID']         =   $genericCampaignData->generic_campaign_id;
                    $output['activeCampaignIDSource']   =   'market_generic';
                }

            }
            else
            {
                $output     =   "Please, add configuration data for the market identified by ID: " .$marketID. ".";
            }
        }
        else
        {
            $output     =   "Please, add configuration data for at least one market.";
        }

        return $output;
    }

    public static function getGenericCampaignData($marketID)
    {
        $cacheStuff =   FALSE;

        //if (Cache::has('generic_campaign_id_' . $marketID))
        //{
        //    return ['generic_campaign_id' => Cache::get('generic_campaign_id_' . $marketID)];
        //}


        if(!$cacheStuff)
        {
            $genericCampaignData = DB::select( DB::raw("SELECT generic_campaign_id FROM markets_config_social_store WHERE market_id = $marketID") );
            $object =   $genericCampaignData[0];
            Cache::put('generic_campaign_id_' . $marketID, $object->generic_campaign_id, 60*24);

            return $genericCampaignData[0];
        }
    }
}