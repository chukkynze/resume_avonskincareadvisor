<?php
/**
 * Project  : avon-scd-api
 *
 * User     : Chukky Nze <chukky.nze@hyfn.com>
 * Date     : 4/17/15
 * Time     : 11:55 AM
 * 
 * Copyright 2015 Hyfn.com All Rights Reserved
 */

namespace App\Models\Api\V1;

/**
 * App\Models\Api\V1\Question
 *
 * @property integer $id 
 * @property integer $market_id 
 * @property integer $quiz_id 
 * @property integer $rep_id 
 * @property string $desktop_background_img 
 * @property string $results_page_title 
 * @property string $results_page_sub_title 
 * @property integer $results_page_order 
 * @property boolean $answers_have_thumbnails 
 * @property boolean $bundle_products_into_regimen 
 * @property boolean $more_than_one_answer 
 * @property boolean $is_branch_question 
 * @property boolean $is_comprehensive_question 
 * @property boolean $is_simple_question 
 * @property boolean $is_path_a 
 * @property boolean $is_path_b 
 * @property integer $comprehensive_list_order 
 * @property integer $simple_list_order 
 * @property integer $path_a_order 
 * @property integer $path_b_order 
 * @property boolean $is_single_product 
 * @property boolean $is_multiple_product_regimen 
 * @property boolean $is_multiple_product_no_regimen 
 * @property integer $layout_id 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Question whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Question whereMarketId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Question whereQuizId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Question whereRepId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Question whereDesktopBackgroundImg($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Question whereResultsPageTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Question whereResultsPageSubTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Question whereResultsPageOrder($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Question whereAnswersHaveThumbnails($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Question whereBundleProductsIntoRegimen($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Question whereMoreThanOneAnswer($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Question whereIsBranchQuestion($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Question whereIsComprehensiveQuestion($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Question whereIsSimpleQuestion($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Question whereIsPathA($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Question whereIsPathB($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Question whereComprehensiveListOrder($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Question whereSimpleListOrder($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Question wherePathAOrder($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Question wherePathBOrder($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Question whereIsSingleProduct($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Question whereIsMultipleProductRegimen($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Question whereIsMultipleProductNoRegimen($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Question whereLayoutId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Question whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\Question whereUpdatedAt($value)
 */
class Question extends BaseApiModel
{

    protected $table        =   'scd_questions';
    protected $primaryKey   =   'id';
    protected $guarded      =   ['id'];
    protected $fillable     =   [
                                    'market_id',
                                    'quiz_id',
                                    'rep_id',

                                    'desktop_background_img',

                                    'results_page_title',
                                    'results_page_sub_title',
                                    'results_page_order',

                                    'answers_have_thumbnails',
                                    'bundle_products_into_regimen',
                                    'more_than_one_answer',

                                    'is_branch_question',
                                    'is_comprehensive_question',
                                    'is_simple_question',

                                    'is_path_a',
                                    'is_path_b',

                                    'comprehensive_list_order',
                                    'simple_list_order',

                                    'path_a_order',
                                    'path_b_order',

                                    'is_single_product',
                                    'is_multiple_product_regimen',
                                    'is_multiple_product_no_regimen',

                                    'layout_id',

                                ];

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getMarketId()
    {
        return $this->market_id;
    }

    /**
     * @param int $market_id
     */
    public function setMarketId($market_id)
    {
        $this->market_id = $market_id;
    }

    /**
     * @return int
     */
    public function getQuizId()
    {
        return $this->quiz_id;
    }

    /**
     * @param int $quiz_id
     */
    public function setQuizId($quiz_id)
    {
        $this->quiz_id = $quiz_id;
    }

    /**
     * @return int
     */
    public function getRepId()
    {
        return $this->rep_id;
    }

    /**
     * @param int $rep_id
     */
    public function setRepId($rep_id)
    {
        $this->rep_id = $rep_id;
    }

    /**
     * @return string
     */
    public function getDesktopBackgroundImg()
    {
        return $this->desktop_background_img;
    }

    /**
     * @param string $desktop_background_img
     */
    public function setDesktopBackgroundImg($desktop_background_img)
    {
        $this->desktop_background_img = $desktop_background_img;
    }

    /**
     * @return string
     */
    public function getResultsPageTitle()
    {
        return $this->results_page_title;
    }

    /**
     * @param string $results_page_title
     */
    public function setResultsPageTitle($results_page_title)
    {
        $this->results_page_title = $results_page_title;
    }

    /**
     * @return string
     */
    public function getResultsPageSubTitle()
    {
        return $this->results_page_sub_title;
    }

    /**
     * @param string $results_page_sub_title
     */
    public function setResultsPageSubTitle($results_page_sub_title)
    {
        $this->results_page_sub_title = $results_page_sub_title;
    }

    /**
     * @return int
     */
    public function getResultsPageOrder()
    {
        return $this->results_page_order;
    }

    /**
     * @param int $results_page_order
     */
    public function setResultsPageOrder($results_page_order)
    {
        $this->results_page_order = $results_page_order;
    }

    /**
     * @return boolean
     */
    public function isAnswersHaveThumbnails()
    {
        return $this->answers_have_thumbnails;
    }

    /**
     * @param boolean $answers_have_thumbnails
     */
    public function setAnswersHaveThumbnails($answers_have_thumbnails)
    {
        $this->answers_have_thumbnails = $answers_have_thumbnails;
    }

    /**
     * @return boolean
     */
    public function isBundleProductsIntoRegimen()
    {
        return $this->bundle_products_into_regimen;
    }

    /**
     * @param boolean $bundle_products_into_regimen
     */
    public function setBundleProductsIntoRegimen($bundle_products_into_regimen)
    {
        $this->bundle_products_into_regimen = $bundle_products_into_regimen;
    }

    /**
     * @return boolean
     */
    public function isMoreThanOneAnswer()
    {
        return $this->more_than_one_answer;
    }

    /**
     * @param boolean $more_than_one_answer
     */
    public function setMoreThanOneAnswer($more_than_one_answer)
    {
        $this->more_than_one_answer = $more_than_one_answer;
    }

    /**
     * @return boolean
     */
    public function isIsBranchQuestion()
    {
        return $this->is_branch_question;
    }

    /**
     * @param boolean $is_branch_question
     */
    public function setIsBranchQuestion($is_branch_question)
    {
        $this->is_branch_question = $is_branch_question;
    }

    /**
     * @return boolean
     */
    public function isIsComprehensiveQuestion()
    {
        return $this->is_comprehensive_question;
    }

    /**
     * @param boolean $is_comprehensive_question
     */
    public function setIsComprehensiveQuestion($is_comprehensive_question)
    {
        $this->is_comprehensive_question = $is_comprehensive_question;
    }

    /**
     * @return boolean
     */
    public function isIsSimpleQuestion()
    {
        return $this->is_simple_question;
    }

    /**
     * @param boolean $is_simple_question
     */
    public function setIsSimpleQuestion($is_simple_question)
    {
        $this->is_simple_question = $is_simple_question;
    }

    /**
     * @return boolean
     */
    public function isIsPathA()
    {
        return $this->is_path_a;
    }

    /**
     * @param boolean $is_path_a
     */
    public function setIsPathA($is_path_a)
    {
        $this->is_path_a = $is_path_a;
    }

    /**
     * @return boolean
     */
    public function isIsPathB()
    {
        return $this->is_path_b;
    }

    /**
     * @param boolean $is_path_b
     */
    public function setIsPathB($is_path_b)
    {
        $this->is_path_b = $is_path_b;
    }

    /**
     * @return int
     */
    public function getComprehensiveListOrder()
    {
        return $this->comprehensive_list_order;
    }

    /**
     * @param int $comprehensive_list_order
     */
    public function setComprehensiveListOrder($comprehensive_list_order)
    {
        $this->comprehensive_list_order = $comprehensive_list_order;
    }

    /**
     * @return int
     */
    public function getSimpleListOrder()
    {
        return $this->simple_list_order;
    }

    /**
     * @param int $simple_list_order
     */
    public function setSimpleListOrder($simple_list_order)
    {
        $this->simple_list_order = $simple_list_order;
    }

    /**
     * @return int
     */
    public function getPathAOrder()
    {
        return $this->path_a_order;
    }

    /**
     * @param int $path_a_order
     */
    public function setPathAOrder($path_a_order)
    {
        $this->path_a_order = $path_a_order;
    }

    /**
     * @return int
     */
    public function getPathBOrder()
    {
        return $this->path_b_order;
    }

    /**
     * @param int $path_b_order
     */
    public function setPathBOrder($path_b_order)
    {
        $this->path_b_order = $path_b_order;
    }

    /**
     * @return boolean
     */
    public function isIsSingleProduct()
    {
        return $this->is_single_product;
    }

    /**
     * @param boolean $is_single_product
     */
    public function setIsSingleProduct($is_single_product)
    {
        $this->is_single_product = $is_single_product;
    }

    /**
     * @return boolean
     */
    public function isIsMultipleProductRegimen()
    {
        return $this->is_multiple_product_regimen;
    }

    /**
     * @param boolean $is_multiple_product_regimen
     */
    public function setIsMultipleProductRegimen($is_multiple_product_regimen)
    {
        $this->is_multiple_product_regimen = $is_multiple_product_regimen;
    }

    /**
     * @return boolean
     */
    public function isIsMultipleProductNoRegimen()
    {
        return $this->is_multiple_product_no_regimen;
    }

    /**
     * @param boolean $is_multiple_product_no_regimen
     */
    public function setIsMultipleProductNoRegimen($is_multiple_product_no_regimen)
    {
        $this->is_multiple_product_no_regimen = $is_multiple_product_no_regimen;
    }

    /**
     * @return int
     */
    public function getLayoutId()
    {
        return $this->layout_id;
    }

    /**
     * @param int $layout_id
     */
    public function setLayoutId($layout_id)
    {
        $this->layout_id = $layout_id;
    }

    /**
     * @return \Carbon\Carbon
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param \Carbon\Carbon $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return \Carbon\Carbon
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param \Carbon\Carbon $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }




}