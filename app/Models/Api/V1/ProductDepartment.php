<?php
/**
 * Project  : avon-scd-api
 *
 * User     : Chukky Nze <chukky.nze@hyfn.com>
 * Date     : 4/17/15
 * Time     : 11:56 AM
 * 
 * Copyright 2015 Hyfn.com All Rights Reserved
 */

namespace App\Models\Api\V1;

/**
 * App\Models\Api\V1\ProductDepartment
 *
 * @property integer $id 
 * @property integer $market_id 
 * @property integer $product_id 
 * @property integer $feed_id 
 * @property integer $campaign_id 
 * @property string $name 
 * @property string $child_names 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductDepartment whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductDepartment whereMarketId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductDepartment whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductDepartment whereFeedId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductDepartment whereCampaignId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductDepartment whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductDepartment whereChildNames($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductDepartment whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Api\V1\ProductDepartment whereUpdatedAt($value)
 */
class ProductDepartment extends BaseApiModel
{
    protected $table        =   'scd_product_departments';
    protected $primaryKey   =   'id';
    protected $guarded      =   ['id'];
    protected $fillable     =   [
                                    'market_id',

                                    'product_id',
                                    'feed_id',
                                    'campaign_id',

                                    'name',
                                    'child_names',
                                ];

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getMarketId()
    {
        return $this->market_id;
    }

    /**
     * @param int $market_id
     */
    public function setMarketId($market_id)
    {
        $this->market_id = $market_id;
    }

    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->product_id;
    }

    /**
     * @param int $product_id
     */
    public function setProductId($product_id)
    {
        $this->product_id = $product_id;
    }

    /**
     * @return int
     */
    public function getFeedId()
    {
        return $this->feed_id;
    }

    /**
     * @param int $feed_id
     */
    public function setFeedId($feed_id)
    {
        $this->feed_id = $feed_id;
    }

    /**
     * @return int
     */
    public function getCampaignId()
    {
        return $this->campaign_id;
    }

    /**
     * @param int $campaign_id
     */
    public function setCampaignId($campaign_id)
    {
        $this->campaign_id = $campaign_id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getChildNames()
    {
        return $this->child_names;
    }

    /**
     * @param string $child_names
     */
    public function setChildNames($child_names)
    {
        $this->child_names = $child_names;
    }

    /**
     * @return \Carbon\Carbon
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param \Carbon\Carbon $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return \Carbon\Carbon
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param \Carbon\Carbon $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }


}