<?php
/**
 * Project  : avon-scd-api
 *
 * User     : Chukky Nze <chukky.nze@hyfn.com>
 * Date     : 4/17/15
 * Time     : 11:58 AM
 * 
 * Copyright 2015 Hyfn.com All Rights Reserved
 */

namespace App\Models\Api\V1;


/**
 * App\Models\Api\V1\QuizArrangement
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Api\V1\Answer[] $answers 
 * @property-read \App\Models\Api\V1\QuestionType $question_types 
 */
class QuizArrangement extends BaseApiModel
{
    protected $table        =   'scd_quiz_arrangement';
    protected $primaryKey   =   'id';
    protected $guarded      =   ['id'];
    protected $fillable     =   [
                                    'market_id',
                                    'quiz_id',
                                    'question_id',
                                    'answer_id',
                                    'product_group_id',
                                    'order',
                                ];



    public function answers()
    {
        return $this->hasMany('App\Models\Api\V1\Answer');
    }

    public function question_types()
    {
        return $this->hasOne('App\Models\Api\V1\QuestionType');
    }
}