<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

/**
 * App\Models\BaseModel
 *
 */
class BaseModel extends Model
{
    use ValidatingTrait;

}
