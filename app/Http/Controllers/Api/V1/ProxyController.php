<?php
/**
 * Project  : avon-scd-api
 *
 * User     : Keith Mayoral <keith@hyfn.com>
 * Date     : 6/29/15
 * Time     : 4:16 PM
 * 
 * Copyright 2015 Hyfn.com All Rights Reserved
 */

namespace App\Http\Controllers\Api\V1;

use Illuminate\Routing\Controller as Controller;
use Request;
use Response;
use App;

class ProxyController extends Controller
{   

    const DEV_API_URL             = "https://qaf.shopapi.avon.com/4/";
    const DEV_API_KEY             = "3C41F4BF-666A-4E42-BDA2-5F86DB109A5B";
    const PROD_API_URL            = "https://shopapi.avon.com/4/";
    const PROD_API_KEY            = "3C41F4BF-666A-4E42-BDA2-5F86DB109A5B";
    private $debug;
    private $environment;    
    private $brand;
    private $language;
    private $api_url;
    private $api_key;
    private $last_curl_error;
    private $last_request_headers;
    private $last_response_headers;



    public function __construct()
    {        
        $this->debug        = Request::input('debug') === 1;
        $this->environment  = Request::input('environment') ?: 'dev';
        $this->brand        = Request::input('brand') ?: 'Avon';
        $this->language     = Request::input('language') ?: 'English';
        $this->api_url      = $this->environment == 'dev' ? self::DEV_API_URL : self::PROD_API_URL;
        $this->api_key      = $this->environment == 'dev' ? self::DEV_API_KEY : self::PROD_API_KEY;
    }

    /*****************************************************************
    ********************** Cart Operations ***************************
    ******************************************************************/

    public function addToCart() 
    {
        $data = Request::input();
        $shopperID = isset($data['shopperid']) ? $data['shopperid']: '';
        $method = 'cart';
        $operation = 'add'; //qafv4
        $type = 'POST';
        $fullUrl = $this->generateFullUrl($method, $operation);
        $requestData = [
            'method'    => $method,
            'operation' => $operation,
            'fullUrl'   => $fullUrl,
            'fields'    => isset($data['fields']) ? $data['fields'] : [],
            'shopperid' => $shopperID,
            'type'      => $type,
        ];      
        unset($data['fields']);
        $requestData['other_fields'] = $data;

        // Create handler that will process the response array  
        $response_handler_callback = function($result) use ($requestData)
        {
            if (isset($result['CurlResponseHeaders']) && isset($result['CurlResponseHeaders']['shopperId']))
            {
                $cartlink = 'http://' . ($this->environment == "dev" ? 'qaf.':'www.') .'avon.com/shop/transfercart.axd?redirect=/bag&inm=skincarediag&shopperid=' . $result['CurlResponseHeaders']['shopperId'];
                if (isset($requestData['other_fields']['repid'])) 
                {
                    $cartlink .= '&repid='. $requestData['other_fields']['repid'];
                }
                $result['Cartlink'] = $cartlink;
            }
            return $result;
        };

        return $this->proxyJsonRequest($fullUrl, $method, $requestData, $response_handler_callback);
    }    


    public function getCurrentCampaign()
    {
        $data = Request::input();
        $shopperID = isset($data['shopperid']) ? $data['shopperid']: '';
        $method = 'cart';
        $operation = 'campaign'; //qafv4
        $type = 'GET';
        $fullUrl = $this->generateFullUrl($method, $operation);
        $requestData = [
            'method'    => $method,
            'operation' => $operation,
            'fullUrl'   => $fullUrl,
            'fields'    => isset($data['fields']) ? $data['fields'] : [],
            'shopperid' => $shopperID,
            'type'      => $type,
        ];      
        unset($data['fields']);
        $requestData['other_fields'] = $data;

        return $this->proxyJsonRequest($fullUrl, $method, $requestData);
    }

    /*****************************************************************
    ********************** Customer Operations ***********************
    ******************************************************************/

    public function authenticateCustomer()
    {
        $data = Request::input();
        $shopperID = isset($data['shopperid']) ? $data['shopperid']: '';
        $method = 'customer';
        $operation = 'authenticate'; //qafv4
        $type = 'POST';
        $fullUrl = $this->generateFullUrl($method, $operation);
        $requestData = [
            'method'    => $method,
            'operation' => $operation,
            'fullUrl'   => $fullUrl,
            'fields'    => isset($data['fields']) ? $data['fields'] : [],
            'usertype'    => isset($data['usertype']) ? $data['usertype'] : '',
            'username'    => isset($data['username']) ? $data['username'] : '',
            'password'    => isset($data['password']) ? $data['password'] : '',
            'shopperid' => $shopperID,
            'type'      => $type,
        ];      
        unset($data['fields']);
        $requestData['other_fields'] = $data;

        return $this->proxyJsonRequest($fullUrl, $method, $requestData);
    }

    public function getCustomerDetails()
    {
        $data = Request::input();
        $shopperID = isset($data['shopperid']) ? $data['shopperid']: '';
        $method = 'customer';
        $operation = 'GetCustomerDetails'; //qafv4
        $type = 'GET';
        $fullUrl = $this->generateFullUrl($method, $operation);
        $requestData = [
            'method'    => $method,
            'operation' => $operation,
            'fullUrl'   => $fullUrl,
            'fields'    => isset($data['fields']) ? $data['fields'] : [],
            'usertype'    => isset($data['usertype']) ? $data['usertype'] : '',
            'username'    => isset($data['username']) ? $data['username'] : '',
            'password'    => isset($data['password']) ? $data['password'] : '',
            'auth_token'    => isset($data['auth_token']) ? $data['auth_token'] : '',
            'shopperid' => $shopperID,
            'type'      => $type,
        ];      
        unset($data['fields']);
        $requestData['other_fields'] = $data;

        return $this->proxyJsonRequest($fullUrl, $method, $requestData);
    }

    public function verifyLogin()
    {
                $data = Request::input();
        $shopperID = isset($data['shopperid']) ? $data['shopperid']: '';
        $method = 'customer';
        $operation = 'singlesignon/verify'; //qafv4
        $type = 'POST';
        $fullUrl = $this->generateFullUrl($method, $operation);
        $requestData = [
            'method'    => $method,
            'operation' => $operation,
            'fullUrl'   => $fullUrl,
            'fields'    => isset($data['fields']) ? $data['fields'] : [],
            'usertype'    => isset($data['usertype']) ? $data['usertype'] : '',
            'username'    => isset($data['username']) ? $data['username'] : '',
            'password'    => isset($data['password']) ? $data['password'] : '',
            'auth_token'    => isset($data['auth_token']) ? $data['auth_token'] : '',
            'shopperid' => $shopperID,
            'type'      => $type,
        ];      
        unset($data['fields']);
        $requestData['other_fields'] = $data;

        return $this->proxyJsonRequest($fullUrl, $method, $requestData);
    }

    /*****************************************************************
    ********************** Private Functions *************************
    ******************************************************************/

    private function generateFullUrl($method, $operation = '', $request = '') {        
        $url = $this->api_url.$method;
        if ($operation != '') 
        {
            $url .= '/'.$operation;
        }
        $url .= $request;
        return $url;
    }


    private function proxyJsonRequest($fullUrl, $method, $data, $response_format_handler = null) 
    {
        $method = $data['method'];
        $operation = $data['operation'];
        // $data = Request::input();

        // Perform any simple validation if we need to here
        if (!isset($data))
        {
            header('Content-Type: application/json');
            echo('{"proxyRequest":{"code":"100", "errors":[{"methodName":"'.$method.'", "errMsg":"Validation errors encourntered, missing required fields.", "errCd":"100"}], "success":"false"}}');
            return false;
        }

        // Make the request
        $result = $this->makeRequest($fullUrl, $method, $data);
        // Return the request response
        if ( $result !== false ) {     
            if (isset($response_format_handler)) {

                $result = $response_format_handler($result);                
            }       
            $response = Response::json($result, 200);
            $response->header('Content-Type', 'application/json');
            return $response;
        } else {
            $error_result = [
                'error_message' =>  "Internal CURL call failed. Reported Curl Error: '".$this->last_curl_error."'",
                'fullUrl' => $fullUrl,
                'method' => $method,
                'data' => $data,
            ];
            $error_result['CurlRequestHeaders'] = $this->last_request_headers;
            $error_result['CurlResponseHeaders'] = $this->last_response_headers;
            $response = Response::json($error_result, 400);
            $response->header('Content-Type', 'application/json');
            
            return $response;
        }
    }

    private function makeRequest($fullUrl, $method, $requestData)
    {
        $this->last_request_headers = [];
        $this->last_response_headers = [];
        $request = $requestData['fields'];
        //$requestString = http_build_query($request);
        $httpHeader = [
            'brand: '.$this->brand,
            'apiKey: '.$this->api_key,
            'language: '.$this->language,
            'Content-Type: application/json',
            //'Content-Length: ' . strlen($request),
        ];
        if (isset($requestData['shopperid'])) {
            $httpHeader[] = "shopperid: {$requestData['shopperid']}";            
        }
        if (isset($requestData['auth_token'])) {
            $httpHeader[] = "auth_token: {$requestData['auth_token']}";            
        }
        if (isset($requestData['usertype'])) {
            $httpHeader[] = "usertype: {$requestData['usertype']}";            
        }
        if (isset($requestData['username'])) {
            $httpHeader[] = "username: {$requestData['username']}";            
        }
        if (isset($requestData['password'])) {
            $httpHeader[] = "password: {$requestData['password']}";            
        }

        $curlOptions = [
            CURLOPT_URL => $fullUrl,
            CURLOPT_CUSTOMREQUEST => $requestData['type'] ?: 'GET',
            CURLOPT_HTTPHEADER => $httpHeader,
            CURLOPT_POSTFIELDS => $request,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER => 1,
            CURLOPT_VERBOSE => true,
            CURLINFO_HEADER_OUT => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_TIMEOUT => 15
        ];

        //call api and get results
        $curl_handle=curl_init();
        curl_setopt_array($curl_handle, $curlOptions);
        $result = curl_exec($curl_handle);        
        $request_header = curl_getinfo($curl_handle, CURLINFO_HEADER_OUT);
        $code = curl_getinfo($curl_handle, CURLINFO_HTTP_CODE);
        $contenttype = curl_getinfo($curl_handle, CURLINFO_CONTENT_TYPE);
        $header_size = curl_getinfo($curl_handle, CURLINFO_HEADER_SIZE);
        $response_header = substr($result, 0, $header_size);
        $body = substr($result, $header_size);

        $this->last_request_headers = $request_headers = $this->parseHeaders($request_header);
        $this->last_response_headers = $response_headers = $this->parseHeaders($response_header, "response");

        if (null !== ($result_json = json_decode($body, true)))
        {
            $result_json['CurlRequestHeaders'] = $request_headers;
            $result_json['CurlResponseHeaders'] = $response_headers;
            //$body = json_encode($result_json);
        }
        else 
        {
            $result_json = false;
        }
        // set body to array 
        $body = $result_json;

        $this->last_curl_error = $result === false ? curl_error($curl_handle) : '';
        curl_close($curl_handle); 

        return $body;
    }

    private function parseHeaders($headers, $header_type = "request") 
    {
        $headers_array = [];
        if (mb_strlen(trim($headers)) > 0) 
        {
            foreach(explode("\r\n", $headers) as $i => $line) 
            {
                if ($i === 0) 
                {                    
                    switch ($header_type) {
                        case 'request':
                            $key = 'Request';
                            break;
                        case 'response':
                        default:
                            $key = 'Http-Code';
                            break;
                    }
                    $headers_array[$key] = $line;
                }
                else if (mb_strlen(trim($line)) > 0)
                {                    
                    list ($key, $value) = explode(': ', $line);
                    $headers_array[$key] = $value;
                }
            }
        }
        return $headers_array;
    }
}