<?php
/**
 * Project  : avon-scd-api
 *
 * User     : Chukky Nze <chukky.nze@hyfn.com>
 * Date     : 4/10/15
 * Time     : 5:33 PM
 * 
 * Copyright 2015 Hyfn.com All Rights Reserved
 */

namespace App\Http\Controllers\Api\V1;


use App\Http\Controllers\Controller;
use Request;
use App\Libraries\AvonAPI;

class BaseApiController extends Controller
{

    protected $campaignID;
    protected $campaignIDSource;

    public function __construct()
    {

    }


    /**
     * @author Chukky Nze <chukky.nze@hyfn.com>
     *
     * @param array     $data
     * @param integer   $code
     * @param string    $format
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function makeResponse($data, $code=200, $format='json')
    {
        if (getenv('APP_ENV') == 'local')
        //if (getenv('APP_ENV') == 'development')
        //if (getenv('APP_ENV') == 'staging')
        //if (getenv('APP_ENV') == 'production')
        {
            // .. add some local stuff to $data
        }

        $finalData  =   $data;

        return AvonAPI::makeAvonResponse($finalData, $code, $format);
    }

    public function extractFileFromURL($imageURL)
    {
        return substr($imageURL, (strrpos($imageURL, '/'))+1);
    }
}