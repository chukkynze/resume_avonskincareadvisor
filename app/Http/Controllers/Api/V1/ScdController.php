<?php
/**
 * Project  : avon-scd-api
 *
 * User     : Chukky Nze <chukky.nze@hyfn.com>
 * Date     : 4/15/15
 * Time     : 4:10 PM
 * 
 * Copyright 2015 Hyfn.com All Rights Reserved
 */

namespace App\Http\Controllers\Api\V1;

use Request;
use Schema;

use App\Models\Api\V1\Quiz;
use App\Models\Api\V1\Question;
use App\Models\Api\V1\Answer;
use App\Models\Api\V1\ProductGroup;
use App\Models\Api\V1\AnswerProduct;

use App\Models\Api\V1\Product;
use App\Models\Api\V1\ProductCampaignDetails;
use App\Models\Api\V1\ProductDepartment;
use App\Models\Api\V1\ProductPromotion;
use App\Models\Api\V1\ProductReview;
use App\Models\Api\V1\ProductVariant;

use App\Models\Api\V1\Icon;
use App\Models\Api\V1\ProductUsage;

use App\Models\Api\V1\QuizUser;
use App\Models\Api\V1\QuizResult;

use App\Models\Api\V1\ScdConfig;

class ScdController extends BaseApiController
{
    protected $quiz;
    protected $question;
    protected $answer;
    protected $productGroup;
    protected $answerProduct;

    protected $product;
    protected $productCampaignDetails;
    protected $productDepartment;
    protected $productPromotion;
    protected $productReview;
    protected $productVariant;

    protected $icon;
    protected $productUsage;

    protected $quizUser;
    protected $quizResult;

    public function __construct
    (
        Quiz                    $quiz,
        Question                $question,
        Answer                  $answer,
        ProductGroup            $productGroup,
        AnswerProduct           $answerProduct,

        Product                 $product,
        ProductCampaignDetails  $productCampaignDetails,
        ProductDepartment       $productDepartment,
        ProductPromotion        $productPromotion,
        ProductReview           $productReview,
        ProductVariant          $productVariant,

        Icon                    $icon,
        ProductUsage            $productUsage,

        QuizUser                $quizUser,
        QuizResult              $quizResult
    )
    {
        parent::__construct();

        $this->quiz                     =   $quiz;
        $this->question                 =   $question;
        $this->answer                   =   $answer;
        $this->productGroup             =   $productGroup;
        $this->answerProduct            =   $answerProduct;

        $this->product                  =   $product;
        $this->productCampaignDetails   =   $productCampaignDetails;
        $this->productDepartment        =   $productDepartment;
        $this->productPromotion         =   $productPromotion;
        $this->productReview            =   $productReview;
        $this->productVariant           =   $productVariant;

        $this->icon                     =   $icon;
        $this->productUsage             =   $productUsage;

        $this->quizUser                 =   $quizUser;
        $this->quizResult               =   $quizResult;

        $campaignIDData                 =   $this->getActiveCampaignID();
        $this->campaignID               =   $campaignIDData['campaignID'];
        $this->campaignIDSource         =   $campaignIDData['campaignIDSource'];
    }

    public function getActiveCampaignID()
    {
        $campaignID         =   NULL;
        $campaignIDSource   =   '';

        $marketID       =   Request::input('marketId');
        $configData     =   ScdConfig::getConfigData($marketID);

        if(isset($configData['rep_campaign_source']))
        {
            switch($configData['rep_campaign_source'])
            {
                case 'dsi'              :   $campaignID         =   NULL;
                    $campaignIDSource   =   '';
                    break;
                case 'get_rep_info'     :   $campaignID         =   NULL;
                    $campaignIDSource   =   '';
                    break;

                default :
                    $campaignID         =   $configData['generic_campaign_id'];
                    $campaignIDSource   =   'market_generic';
            }

            if(isset($campaignID))
            {
                $finalOutput    =   [
                    'campaignID'        =>  $this->formatCampaignID($campaignID),
                    'campaignIDSource'  =>  $campaignIDSource,
                ];
            }
            else
            {
                $finalOutput    =   [
                    'campaignID'        =>  $this->formatCampaignID($configData['generic_campaign_id']),
                    'campaignIDSource'  =>  'market_generic',
                ];
            }
        }
        else
        {
            $finalOutput    =   [
                'campaignID'        =>  $this->formatCampaignID($configData['generic_campaign_id']),
                'campaignIDSource'  =>  'market_generic',
            ];
        }

        return $finalOutput;
    }


    public function formatCampaignID($rawCampaignID)
    {
        $ammo   =   explode('-', $rawCampaignID);
        return $ammo[1].$ammo[0];
    }


    /**
     * First call to get things all setup
     *
     * @author Chukky Nze <chukky.nze@hyfn.com>
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @example     http://vm.avon-scd-api.local/api/v1/init-scd-api?devKey=b699bf8eadbc24d5c74322c783bb47f9&marketId=174&repPubKey=1e5bc474b25dbd2d40b2b6bf2c99089f&shopperId=1&customerId=1&parentUrl=https://www.google.com&XDEBUG_SESSION_START=PHPSTORM
     */
    public function initApiTool()
    {
        $marketID   =   Request::input('marketId');

        if(isset($marketID) && is_numeric($marketID))
        {

            if (Schema::hasTable('scd_quiz'))
            {
                if (Schema::hasTable('markets_config_scdtool'))
                {
                    // Quiz
                    $Quiz   =   $this->quiz->whereMarketId($marketID)->first();

                    if(isset($Quiz) && $Quiz->isValid())
                    {
                        // Branch Questions
                        $quizBranchQuestion     =   $this->question->
                                                    whereMarketId($marketID)->
                                                    whereIsBranchQuestion(1)->
                                                    first();

                        // Answers to Branch Question
                        $BranchAnswersData      =   [];
                        $BranchAnswers          =   $this->answer->whereQuestionId($quizBranchQuestion->getId())->get();
                        foreach ($BranchAnswers as $answer)
                        {
                            $BranchAnswersData[]  =
                                [
                                    'id'                    =>  $answer->getId(),
                                    'question_id'           =>  $answer->getQuestionId(),
                                    'text'                  =>  $answer->getText(),
                                    'thumbnail_url'         =>  $answer->getThumbnailUrl(),
                                    'thumbnail_url_file'    =>  $this->extractFileFromURL($answer->getThumbnailUrl()),
                                    'results_page_copy'     =>  $answer->getResultsPageCopy(),
                                    'is_path_a'             =>  $answer->getIsPathA(),
                                    'is_path_b'             =>  $answer->getIsPathB(),
                                    'order'                 =>  $answer->getOrder(),
                                    'created_at'            =>  $answer->getCreatedAt(),
                                    'updated_at'            =>  $answer->getUpdatedAt(),
                                ];
                        }


                        $quizBranchQuestionData =   [
                            'id'                                =>  $quizBranchQuestion->getId(),
                            'market_id'                         =>  $quizBranchQuestion->getMarketId(),
                            'quiz_id'                           =>  $quizBranchQuestion->getQuizId(),
                            'rep_id'                            =>  $quizBranchQuestion->getRepId(),
                            'desktop_background_img'            =>  $quizBranchQuestion->getDesktopBackgroundImg(),
                            'desktop_background_img_file'       =>  $this->extractFileFromURL($quizBranchQuestion->getDesktopBackgroundImg()),
                            'results_page_title'                =>  $quizBranchQuestion->getResultsPageTitle(),
                            'results_page_sub_title'            =>  $quizBranchQuestion->getResultsPageSubTitle(),
                            'results_page_order'                =>  $quizBranchQuestion->getResultsPageOrder(),
                            'answers_have_thumbnails'           =>  $quizBranchQuestion->isAnswersHaveThumbnails(),
                            'bundle_products_into_regimen'      =>  $quizBranchQuestion->isBundleProductsIntoRegimen(),
                            'more_than_one_answer'              =>  $quizBranchQuestion->isMoreThanOneAnswer(),
                            'is_branch_question'                =>  $quizBranchQuestion->isIsBranchQuestion(),
                            'is_comprehensive_question'         =>  0,
                            'is_simple_question'                =>  0,
                            'is_path_a'                         =>  $quizBranchQuestion->isIsPathA(),
                            'is_path_b'                         =>  $quizBranchQuestion->isIsPathB(),
                            'comprehensive_list_order'          =>  0,
                            'simple_list_order'                 =>  0,
                            'path_a_order'                      =>  $quizBranchQuestion->getPathAOrder(),
                            'path_b_order'                      =>  $quizBranchQuestion->getPathBOrder(),
                            'is_single_product'                 =>  $quizBranchQuestion->isIsSingleProduct(),
                            'is_multiple_product_regimen'       =>  $quizBranchQuestion->isIsMultipleProductRegimen(),
                            'is_multiple_product_no_regimen'    =>  $quizBranchQuestion->isIsMultipleProductNoRegimen(),
                            'layout_id'                         =>  0,
                            'created_at'                        =>  $quizBranchQuestion->getCreatedAt(),
                            'updated_at'                        =>  $quizBranchQuestion->getUpdatedAt(),

                            'answers'                           =>  $BranchAnswersData,
                        ];

                        // Path A Questions
                        $pathAQuestions     =   $this->question->
                        whereMarketId($marketID)->
                        whereIsBranchQuestion(0)->
                        whereIsPathA(1)->
                        orderBy('path_a_order', 'asc')->
                        get();

                        if(isset($pathAQuestions) && count($pathAQuestions) >= 1)
                        {
                            // Path B questions
                            $pathBQuestions    =   $this->question->
                            whereMarketId($marketID)->
                            whereIsBranchQuestion(0)->
                            whereIsPathB(1)->
                            orderBy('path_b_order', 'asc')->
                            get();

                            // We have all we need at this point

                            $quizQuestions    =   $this->question->
                            whereMarketId($marketID)->
                            whereIsBranchQuestion(0)->
                            get();

                            /*
                             foreach ($pathAQuestions as $question)
                            {
                                $quizQuestions[]    =   $question;
                            }

                            foreach ($pathBQuestions as $question)
                            {
                                $quizQuestions[]    =   $question;
                            }
                             */

                            $quizQuestionsData=[];
                            foreach ($quizQuestions as $question)
                            {
                                $AnswersData    =   [];
                                $Answers        =   $this->answer->whereQuestionId($question->getId())->get();
                                foreach ($Answers as $answer)
                                {
                                    $ProductGroup               =   $this->productGroup->whereAnswerId($answer->getId())->first();
                                    $AnswerProducts             =   $this->answerProduct->whereProductGroupId($ProductGroup->getId())->get();

                                    $AnswerProductsData=[];
                                    foreach ($AnswerProducts as $answerProduct)
                                    {
                                        $AnswerProductsData[]   =
                                            [
                                                'id'                                    =>  $answerProduct->getId(),
                                                'market_id'                             =>  $answerProduct->getMarketId(),
                                                'product_group_id'                      =>  $answerProduct->getProductGroupId(),

                                                'product_id'                            =>  $answerProduct->getProductId(),
                                                'product_id_data'                       =>  $this->getFormattedProduct($answerProduct->getProductId(), (int)$this->campaignID),

                                                'replenish_product_id'                  =>  $answerProduct->getReplenishProductId(),
                                                'replenish_product_id_data'             =>  $this->getFormattedProduct($answerProduct->getReplenishProductId(), (int)$this->campaignID),

                                                'results_page_copy'                     =>  $answerProduct->getResultsPageCopy(),
                                                'results_page_heading'                  =>  $answerProduct->getResultsPageHeading(),

                                                'results_page_step_id'                  =>  $answerProduct->getResultsPageStepId(),
                                                'results_page_step_id_data'             =>  $this->getFormattedIconData($answerProduct->getResultsPageStepId(), 'step'),

                                                'results_page_product_usage_id'         =>  $answerProduct->getResultsPageProductUsageId(),
                                                'results_page_product_usage_id_data'    =>  $this->getFormattedProductUsageData($answerProduct->getResultsPageProductUsageId()),

                                                'results_page_icon_usage_id'            =>  $answerProduct->getResultsPageIconUsageId(),
                                                'results_page_icon_usage_id_data'       =>  $this->getFormattedIconData($answerProduct->getResultsPageIconUsageId(), 'usage'),

                                                'created_at'                            =>  $answerProduct->getCreatedAt(),
                                                'updated_at'                            =>  $answerProduct->getUpdatedAt(),

                                                'results_page_order'                    =>  $answerProduct->getResultsPageOrder(),
                                            ];
                                    }

                                    $ProductGroupData   =
                                        [
                                            'id'                                        =>  $ProductGroup->getId(),
                                            'market_id'                                 =>  $ProductGroup->getMarketId(),
                                            'answer_id'                                 =>  $ProductGroup->getAnswerId(),
                                            'regimen_description'                       =>  $ProductGroup->getRegimenDescription(),
                                            'regimen_image_url'                         =>  $ProductGroup->getRegimenImageUrl(),
                                            'regimen_image_url_file'                    =>  $this->extractFileFromURL($ProductGroup->getRegimenImageUrl()),
                                            'regimen_secondary_image'                   =>  $ProductGroup->getRegimenSecondaryImage(),
                                            'regimen_secondary_image_file'              =>  $this->extractFileFromURL($ProductGroup->getRegimenSecondaryImage()),

                                            'replenish_product_id'                      =>  $ProductGroup->getReplenishProductId(),
                                            'replenish_product_id_data'                 =>  $this->getFormattedProduct($ProductGroup->getReplenishProductId(), (int)$this->campaignID),

                                            'regimen_product_id'                        =>  $ProductGroup->getRegimenProductId(),
                                            'regimen_product_id_data'                   =>  $this->getFormattedProduct($ProductGroup->getRegimenProductId(), (int)$this->campaignID),

                                            'regimen_step_id'                           =>  $ProductGroup->getRegimenStepId(),
                                            'regimen_step_id_data'                      =>  $this->getFormattedIconData($ProductGroup->getRegimenStepId(), 'step'),

                                            'regimen_usage_id'                          =>  $ProductGroup->getRegimenUsageId(),
                                            'regimen_usage_id_data'                     =>  $this->getFormattedIconData($ProductGroup->getRegimenUsageId(), 'usage'),

                                            'regimen_results_product_usage_id'          =>  $ProductGroup->getRegimenResultsPageIconUsageId(),
                                            'regimen_results_product_usage_id_data'     =>  $this->getFormattedProductUsageData($ProductGroup->getRegimenResultsPageIconUsageId()),

                                            'regimen_results_page_order'                =>  $ProductGroup->getRegimenResultsPageOrder(),

                                            'created_at'                                =>  $ProductGroup->getCreatedAt(),
                                            'updated_at'                                =>  $ProductGroup->getUpdatedAt(),

                                            'products'                                  =>  $AnswerProductsData,
                                        ];

                                    $AnswersData[]  =
                                        [
                                            'id'                    =>  $answer->getId(),
                                            'question_id'           =>  $answer->getQuestionId(),
                                            'text'                  =>  $answer->getText(),
                                            'thumbnail_url'         =>  $answer->getThumbnailUrl(),
                                            'thumbnail_url_file'    =>  $this->extractFileFromURL($answer->getThumbnailUrl()),
                                            'results_page_copy'     =>  $answer->getResultsPageCopy(),
                                            'is_path_a'             =>  $answer->getIsPathA(),
                                            'is_path_b'             =>  $answer->getIsPathB(),
                                            'order'                 =>  $answer->getOrder(),
                                            'created_at'            =>  $answer->getCreatedAt(),
                                            'updated_at'            =>  $answer->getUpdatedAt(),

                                            'product_group'         =>  $ProductGroupData,
                                        ];
                                }

                                $quizQuestionsData[]  =
                                    [
                                        'id'                                =>  $question->getId(),
                                        'market_id'                         =>  $question->getMarketId(),
                                        'quiz_id'                           =>  $question->getQuizId(),
                                        'rep_id'                            =>  $question->getRepId(),
                                        'desktop_background_img'            =>  $question->getDesktopBackgroundImg(),
                                        'desktop_background_img_file'       =>  $this->extractFileFromURL($question->getDesktopBackgroundImg()),
                                        'results_page_title'                =>  $question->getResultsPageTitle(),
                                        'results_page_sub_title'            =>  $question->getResultsPageSubTitle(),
                                        'results_page_order'                =>  $question->getResultsPageOrder(),
                                        'answers_have_thumbnails'           =>  $question->isAnswersHaveThumbnails(),
                                        'bundle_products_into_regimen'      =>  $question->isBundleProductsIntoRegimen(),
                                        'more_than_one_answer'              =>  $question->isMoreThanOneAnswer(),
                                        'is_branch_question'                =>  $question->isIsBranchQuestion(),
                                        'is_comprehensive_question'         =>  0,
                                        'is_simple_question'                =>  0,
                                        'is_path_a'                         =>  $question->isIsPathA(),
                                        'is_path_b'                         =>  $question->isIsPathB(),
                                        'comprehensive_list_order'          =>  0,
                                        'simple_list_order'                 =>  0,
                                        'path_a_order'                      =>  $question->getPathAOrder(),
                                        'path_b_order'                      =>  $question->getPathBOrder(),
                                        'is_single_product'                 =>  $question->isIsSingleProduct(),
                                        'is_multiple_product_regimen'       =>  $question->isIsMultipleProductRegimen(),
                                        'is_multiple_product_no_regimen'    =>  $question->isIsMultipleProductNoRegimen(),
                                        'layout_id'                         =>  0,
                                        'created_at'                        =>  $question->getCreatedAt(),
                                        'updated_at'                        =>  $question->getUpdatedAt(),

                                        'answers'                           =>  $AnswersData,
                                    ];
                            }

                            $responseData   =
                                [
                                    'quiz'=>
                                        [
                                            'id'        =>  1,
                                            'market_id' =>  $marketID,
                                            'questions' =>
                                                [
                                                    'branchQuestion'    =>  $quizBranchQuestionData,
                                                    'pathQuestions'     =>  $quizQuestionsData,
                                                ],
                                        ]
                                ];
                            $responseCode   =   200;
                        }
                        else
                        {
                            // No Path A Questions
                            $responseData   =
                                [
                                    'quiz'=>
                                        [
                                            'status'    =>  FALSE,
                                            'message'   =>  'No Path A Questions.',
                                        ]
                                ];
                            $responseCode   =   500;
                        }
                    }
                    else
                    {
                        // Could not locate the current or active quiz
                        $responseData   =
                            [
                                'quiz'=>
                                    [
                                        'status'    =>  FALSE,
                                        'message'   =>  'Could not locate the current or active quiz with that Market ID.',
                                    ]
                            ];
                        $responseCode   =   500;
                    }
                }
                else
                {
                    // The Quiz data table has not been setup yet. Please, add at least one question to get the process started.
                    $responseData   =
                        [
                            'quiz'=>
                                [
                                    'status'    =>  FALSE,
                                    'message'   =>  'The Quiz config table has not been setup yet. Please, add configuration data for at least one market to get the process started.',
                                ]
                        ];
                    $responseCode   =   500;
                }
            }
            else
            {
                // The Quiz data table has not been setup yet. Please, add at least one question to get the process started.
                $responseData   =
                    [
                        'quiz'=>
                            [
                                'status'    =>  FALSE,
                                'message'   =>  'The Quiz data table has not been setup for any market. Please, add at least one question from at least one market to get the process started.',
                            ]
                    ];
                $responseCode   =   500;
            }

        }
        else
        {
            // Bad Market ID
            $responseData   =
                [
                    'quiz'=>
                        [
                            'status'    =>  FALSE,
                            'message'   =>  'This Market ID format is invalid.',
                        ]
                ];
            $responseCode   =   500;
        }






        return $this::makeResponse($responseData, $responseCode);
    }


    /**
     * Format product output according to API specifications of the client
     *
     * @author Chukky Nze <chukky.nze@hyfn.com>
     *
     * @param $productID
     * @param $campaignID
     *
     * @return array
     */
    public function getFormattedProduct($productID, $campaignID)
    {
        $ProductData=[];
        if($productID >= 1)
        {
            $Product            =   $this->product->whereId($productID)->first();
            $ProductData        =   [
                'id'                                =>  $Product->getId(),
                'market_id'                         =>  $Product->getMarketId(),
                'feed_id'                           =>  $Product->getFeedId(),
                'auto_replenishment_caption'        =>  $Product->getAutoReplenishmentCaption(),
                'auto_replenishment_frequencies'    =>  $Product->getAutoReplenishmentFrequencies(),
                'can_buy'                           =>  $Product->isCanBuy(),
                'product_brand'                     =>  $Product->getProductBrand(),
                'profile_number'                    =>  $Product->getProfileNumber(),
                'variant_type'                      =>  $Product->getVariantType(),
                'name'                              =>  $Product->getName(),
                'description'                       =>  $Product->getDescription(),
                'image'                             =>  $Product->getImage(),
                'image_file'                        =>  $this->extractFileFromURL($Product->getImage()),
                'show_price_in_ui'                  =>  $Product->isShowPriceInUi(),
                'is_from_dsi_feed'                  =>  $Product->isIsFromDsiFeed(),
                'is_auto_replenish'                 =>  $Product->isIsAutoReplenish(),
                'is_normal_purchase'                =>  $Product->isIsNormalPurchase(),
                'created_at'                        =>  $Product->getCreatedAt(),
                'updated_at'                        =>  $Product->getUpdatedAt(),
            ];


            $campaignDetails    =   $this->productCampaignDetails->whereProductId($productID)->whereCampaignId((int)$campaignID)->get();
            $departments        =   $this->productDepartment->whereProductId($productID)->whereCampaignId((int)$campaignID)->get();
            $promotions         =   $this->productPromotion->whereProductId($productID)->whereCampaignId((int)$campaignID)->get();
            $reviews            =   $this->productReview->whereProductId($productID)->whereCampaignId((int)$campaignID)->get();
            $variants           =   $this->productVariant->whereProductId($productID)->whereCampaignId((int)$campaignID)->get();

            $campaignDetailsData=[];
            $departmentsData=[];
            $promotionsData=[];
            $reviewsDataData=[];
            $variantsData=[];

            foreach ($campaignDetails as $campaignDetail)
            {
                $campaignDetailsData[]  =   [
                    'id'                        =>  $campaignDetail->getId(),
                    'market_id'                 =>  $campaignDetail->getMarketId(),
                    'product_id'                =>  $campaignDetail->getProductId(),
                    'feed_id'                   =>  $campaignDetail->getFeedId(),
                    'campaign_id'               =>  $campaignDetail->getCampaignId(),
                    'list_price'                =>  $campaignDetail->getListPrice(),
                    'sales_price'               =>  $campaignDetail->getSalesPrice(),
                    'auto_replenishment_price'  =>  $campaignDetail->getAutoReplenishmentPrice(),
                    'created_at'                =>  $campaignDetail->getCreatedAt(),
                    'updated_at'                =>  $campaignDetail->getUpdatedAt(),
                ];
            }

            foreach ($departments as $department)
            {
                $departmentsData[]  =   [
                    'id'            =>  $department->getId(),
                    'market_id'     =>  $department->getMarketId(),
                    'product_id'    =>  $department->getProductId(),
                    'feed_id'       =>  $department->getFeedId(),
                    'campaign_id'   =>  $department->getCampaignId(),
                    'name'          =>  $department->getName(),
                    'child_names'   =>  $department->getChildNames(),
                    'created_at'    =>  $department->getCreatedAt(),
                    'updated_at'    =>  $department->getUpdatedAt(),
                ];
            }

            foreach ($promotions as $promotion)
            {
                $promotionsData[]  =   [
                    'id'            =>  $promotion->getId(),
                    'market_id'     =>  $promotion->getMarketId(),
                    'product_id'    =>  $promotion->getProductId(),
                    'feed_id'       =>  $promotion->getFeedId(),
                    'campaign_id'   =>  $promotion->getCampaignId(),
                    'name'          =>  $promotion->getName(),
                    'description'   =>  $promotion->getDescription(),
                    'created_at'    =>  $promotion->getCreatedAt(),
                    'updated_at'    =>  $promotion->getUpdatedAt(),
                ];
            }

            foreach ($reviews as $review)
            {
                $reviewsDataData[]  =   [
                    'id'                =>  $review->getId(),
                    'market_id'         =>  $review->getMarketId(),
                    'product_id'        =>  $review->getProductId(),
                    'feed_id'           =>  $review->getFeedId(),
                    'campaign_id'       =>  $review->getCampaignId(),
                    'average_rating'    =>  $review->getAverageRating(),
                    'total_reviews'     =>  $review->getTotalReviews(),
                    'created_at'        =>  $review->getCreatedAt(),
                    'updated_at'        =>  $review->getUpdatedAt(),
                ];
            }

            foreach ($variants as $variant)
            {
                $variantsData[]  =   [
                    'id'            =>  $variant->getId(),
                    'market_id'     =>  $variant->getMarketId(),
                    'product_id'    =>  $variant->getProductId(),
                    'feed_id'       =>  $variant->getFeedId(),
                    'campaign_id'   =>  $variant->getCampaignId(),
                    'can_buy'       =>  $variant->isCanBuy(),
                    'fsc'           =>  $variant->getFsc(),
                    'image'         =>  $variant->getImage(),
                    'image_file'    =>  $this->extractFileFromURL($variant->getImage()),
                    'line_number'   =>  $variant->getLineNumber(),
                    'name'          =>  $variant->getName(),
                    'sku'           =>  $variant->getSku(),
                    'is_active'     =>  $variant->isIsActive(),
                    'created_at'    =>  $variant->getCreatedAt(),
                    'updated_at'    =>  $variant->getUpdatedAt(),
                ];
            }


            $ProductData['campaignDetails'] =   $campaignDetailsData;
            $ProductData['departments']     =   $departmentsData;
            $ProductData['promotions']      =   $promotionsData;
            $ProductData['reviews']         =   $reviewsDataData;
            $ProductData['variants']        =   $variantsData;
        }

        return $ProductData;
    }


    /**
     * Format icon output according to API specifications of the client
     *
     * @author Chukky Nze <chukky.nze@hyfn.com>
     *
     * @param $iconID
     * @param $type
     *
     * @return array
     */
    public function getFormattedIconData($iconID, $type)
    {
        $IconData   =   [];
        if($iconID >= 1)
        {
            $Icon       =   $this->icon->whereId($iconID)->whereType($type)->first();
            $IconData   =   [
                'id'                        =>  $Icon->getId(),
                'market_id'                 =>  $Icon->getMarketId(),
                'image_url'                 =>  $Icon->getImageUrl(),
                'image_url_file'            =>  $this->extractFileFromURL($Icon->getImageUrl()),
                'description'               =>  $Icon->getDescription(),
                'type'                      =>  $Icon->getType(),
                'order'                     =>  $Icon->getOrder(),
                'results_page_order_option' =>  $Icon->getResultsPageOrderOption(),
                'created_at'                =>  $Icon->getCreatedAt(),
                'updated_at'                =>  $Icon->getUpdatedAt(),
            ];
        }

        return $IconData;
    }


    /**
     * Format product usage output according to API specifications of the client
     *
     * @author Chukky Nze <chukky.nze@hyfn.com>
     *
     * @param $productUsageID
     * @return array
     */
    public function getFormattedProductUsageData($productUsageID)
    {
        $UsageData  =   [];
        if($productUsageID >= 1)
        {
            $Usage      =   $this->productUsage->whereId($productUsageID)->first();
            $UsageData  = [
                'id'            =>  $Usage->getId(),
                'market_id'     =>  $Usage->getMarketId(),
                'name'          =>  $Usage->getName(),
                'description'   =>  $Usage->getDescription(),
                'created_at'    =>  $Usage->getCreatedAt(),
                'updated_at'    =>  $Usage->getUpdatedAt(),
            ];
        }

        return $UsageData;
    }


    /**
     * Validate the quiz user and perform the actual result save.
     *
     * @author Chukky Nze <chukky.nze@hyfn.com>
     *
     * @param $QuizUser
     * @return array
     *
     * @example
     *
     * http://vm.avon-scd-api.local/api/v1/save-quiz-results
     * ?
     * devKey=b699bf8eadbc24d5c74322c783bb47f9
     * &
     * marketId=174
     * &
     * repPubKey=1e5bc474b25dbd2d40b2b6bf2c99089f
     * &
     * shopperId=1
     * &
     * customerId=1
     * &
     * facebook_id=1
     * &
     * vkontakte_id=1
     * &
     * first_name=Chukky
     * &
     * last_name=Nze
     * &
     * quiz_results=[{\"market_id\":174,\"quiz_user_id\":0,\"quiz_id\":1,\"question_id\":1,\"answer_id\":1,\"product_group_id\":1,\"answer_product_id\":3,\"was_asked\":1,\"was_answered\":1,\"added_to_cart\":1,\"was_bought\":1,\"was_shared\":1},{\"market_id\":174,\"quiz_user_id\":1,\"quiz_id\":1,\"question_id\":2,\"answer_id\":2,\"product_group_id\":2,\"answer_product_id\":3,\"was_asked\":1,\"was_answered\":1,\"added_to_cart\":0,\"was_bought\":0,\"was_shared\":0}]
         * &
     * parentUrl=https://www.google.com
     * &
     * XDEBUG_SESSION_START=PHPSTORM
     *
     *
     *
     * http://vm.avon-scd-api.local/api/v1/save-quiz-results-customer-id?devKey=b699bf8eadbc24d5c74322c783bb47f9&marketId=174&repPubKey=1e5bc474b25dbd2d40b2b6bf2c99089f&shopperId=1&customerId=1&facebook_id=1&vkontakte_id=1&first_name=Chukky&last_name=Nze&quiz_results=[{\"market_id\":174,\"quiz_user_id\":0,\"quiz_id\":1,\"question_id\":1,\"answer_id\":1,\"product_group_id\":1,\"answer_product_id\":3,\"was_asked\":1,\"was_answered\":1,\"added_to_cart\":1,\"was_bought\":1,\"was_shared\":1},{\"market_id\":174,\"quiz_user_id\":1,\"quiz_id\":1,\"question_id\":2,\"answer_id\":2,\"product_group_id\":2,\"answer_product_id\":3,\"was_asked\":1,\"was_answered\":1,\"added_to_cart\":0,\"was_bought\":0,\"was_shared\":0}]&parentUrl=https://www.google.com&XDEBUG_SESSION_START=PHPSTORM
     * http://vm.avon-scd-api.local/api/v1/save-quiz-results-vkontakte-id?devKey=b699bf8eadbc24d5c74322c783bb47f9&marketId=174&repPubKey=1e5bc474b25dbd2d40b2b6bf2c99089f&shopperId=1&customerId=1&facebook_id=1&vkontakte_id=1&first_name=Chukky&last_name=Nze&quiz_results=[{\"market_id\":174,\"quiz_user_id\":0,\"quiz_id\":1,\"question_id\":1,\"answer_id\":1,\"product_group_id\":1,\"answer_product_id\":3,\"was_asked\":1,\"was_answered\":1,\"added_to_cart\":1,\"was_bought\":1,\"was_shared\":1},{\"market_id\":174,\"quiz_user_id\":1,\"quiz_id\":1,\"question_id\":2,\"answer_id\":2,\"product_group_id\":2,\"answer_product_id\":3,\"was_asked\":1,\"was_answered\":1,\"added_to_cart\":0,\"was_bought\":0,\"was_shared\":0}]&parentUrl=https://www.google.com&XDEBUG_SESSION_START=PHPSTORM
     * http://vm.avon-scd-api.local/api/v1/save-quiz-results-facebook-id?devKey=b699bf8eadbc24d5c74322c783bb47f9&marketId=174&repPubKey=1e5bc474b25dbd2d40b2b6bf2c99089f&shopperId=1&customerId=1&facebook_id=1&vkontakte_id=1&first_name=Chukky&last_name=Nze&quiz_results=[{\"market_id\":174,\"quiz_user_id\":0,\"quiz_id\":1,\"question_id\":1,\"answer_id\":1,\"product_group_id\":1,\"answer_product_id\":3,\"was_asked\":1,\"was_answered\":1,\"added_to_cart\":1,\"was_bought\":1,\"was_shared\":1},{\"market_id\":174,\"quiz_user_id\":1,\"quiz_id\":1,\"question_id\":2,\"answer_id\":2,\"product_group_id\":2,\"answer_product_id\":3,\"was_asked\":1,\"was_answered\":1,\"added_to_cart\":0,\"was_bought\":0,\"was_shared\":0}]&parentUrl=https://www.google.com&XDEBUG_SESSION_START=PHPSTORM
     */
    public function validateQuizUser($QuizUser)
    {
        if(isset($QuizUser) && $QuizUser->isValid())
        {
            $responseData   =   $this->saveQuizResults($QuizUser);
        }
        else
        {
            $QuizUser   =   new QuizUser;
            $QuizUser->setMarketId(Request::input('marketId'));

            $QuizUser->setCustomerId(Request::input('customerId')       ?: 0);
            $QuizUser->setFacebookId(Request::input('facebook_id')      ?: 0);
            $QuizUser->setVkontakteId(Request::input('vkontakte_id')    ?: 0);

            $QuizUser->setFirstName(Request::input('first_name'));
            $QuizUser->setLastName(Request::input('last_name')          ?: '');

            $QuizUser->setShopperId(Request::input('shopper_id')        ?: 0);

            if($QuizUser->isValid())
            {
                $QuizUser->save();
                $responseData   =   $this->saveQuizResults($QuizUser);
            }
            else
            {
                $responseData   =
                    [
                        'status'        =>  TRUE,
                        'actionStatus'  =>  FALSE,
                        'message'       =>  'The information for the quiz user provided is invalid.',
                        'errorMsg'      =>  [
                                                'data'      =>  [
                                                                    'market_id'     =>  Request::input('marketId'),

                                                                    'customer_id'   =>  Request::input('customerId')    ?: 0,
                                                                    'facebook_id'   =>  Request::input('facebook_id')   ?: 0,
                                                                    'vkontakte_id'  =>  Request::input('vkontakte_id')  ?: 0,

                                                                    'first_name'    =>  Request::input('first_name'),
                                                                    'last_name'     =>  Request::input('last_name')     ?: '',

                                                                    'shopper_id'    =>  Request::input('shopper_id')    ?: 0,
                                                                ],
                                                'errors'    =>  $QuizUser->getErrors()->toArray(),
                                            ],
                    ];
            }
        }

        return $responseData;
    }


    /**
     * Save quiz results to a user identified by Vkontakte id.
     *
     * @author Chukky Nze <chukky.nze@hyfn.com>
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function saveQuizResultsByVkontakteID()
    {
        $responseData   =   $this->validateQuizUser($this->quizUser->whereVkontakteId(Request::input('vkontakteId') ?: $this->quizUser)->first());

        return $this::makeResponse($responseData, 200);
    }


    /**
     * Save quiz results to a user identified by Facebook id.
     *
     * @author Chukky Nze <chukky.nze@hyfn.com>
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function saveQuizResultsByFacebookID()
    {
        $responseData   =   $this->validateQuizUser($this->quizUser->whereFacebookId(Request::input('facebookId') ?: $this->quizUser)->first());

        return $this::makeResponse($responseData, 200);
    }


    /**
     * Save quiz results to a user identified by customer id.
     *
     * @author Chukky Nze <chukky.nze@hyfn.com>
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function saveQuizResultsByCustomerID()
    {
        $responseData   =   $this->validateQuizUser($this->quizUser->whereCustomerId(Request::input('customerId') ?: $this->quizUser)->first());

        return $this::makeResponse($responseData, 200);
    }


    /**
     * Saves the quiz results associated with the given quiz user identification info.
     *
     * @author Chukky Nze <chukky.nze@hyfn.com>
     *
     * @param QuizUser $QuizUser
     * @return array
     */
    public function saveQuizResults(QuizUser $QuizUser)
    {
        $marketID       =   Request::input('marketId');
        $QuizResults    =   json_decode(urldecode(stripslashes(Request::input('quiz_results'))), true);

        $ValidModels=[];
        $InvalidModels=[];
        foreach ($QuizResults as $quizResult)
        {
            $QuizResultModel    =   new QuizResult();

            $QuizResultModel->setMarketId($marketID);

            $QuizResultModel->setQuizUserId($QuizUser->getId());
            $QuizResultModel->setQuizId($quizResult['quiz_id']);

            $QuizResultModel->setQuestionId($quizResult['question_id']);
            $QuizResultModel->setAnswerId($quizResult['answer_id']);
            $QuizResultModel->setProductGroupId($quizResult['product_group_id']);
            $QuizResultModel->setAnswerProductId($quizResult['answer_product_id']);

            $QuizResultModel->setWasAsked($quizResult['was_asked']);
            $QuizResultModel->setWasAnswered($quizResult['was_answered']);
            $QuizResultModel->setAddedToCart($quizResult['added_to_cart']);
            $QuizResultModel->setWasBought($quizResult['was_bought']);
            $QuizResultModel->setWasShared($quizResult['was_shared']);

            if($QuizResultModel->isValid())
            {
                $ValidModels[]      =   $QuizResultModel;
            }
            else
            {
                $InvalidModels[]    =   [
                                            'data'      =>  [
                                                                'market_id'         =>  $marketID,

                                                                'quiz_user_id'      =>  $QuizUser->getId(),
                                                                'quiz_id'           =>  $quizResult['quiz_id'],

                                                                'question_id'       =>  $quizResult['question_id'],
                                                                'answer_id'         =>  $quizResult['answer_id'],
                                                                'product_group_id'  =>  $quizResult['product_group_id'],
                                                                'answer_product_id' =>  $quizResult['answer_product_id'],

                                                                'was_asked'         =>  $quizResult['was_asked'],
                                                                'was_answered'      =>  $quizResult['was_answered'],
                                                                'added_to_cart'     =>  $quizResult['added_to_cart'],
                                                                'was_bought'        =>  $quizResult['was_bought'],
                                                                'was_shared'        =>  $quizResult['was_shared'],
                                                            ],
                                            'errors'    =>  $QuizResultModel->getErrors()->toArray(),
                                        ];
            }
        }

        if(count($InvalidModels) == 0 && count($ValidModels) == count($QuizResults))
        {
            foreach ($ValidModels as $Model)
            {
                $Model->save();
            }

            $status     =   TRUE;
            $message    =   'A quiz, made up of '
                            .count($ValidModels)
                            . ' result sets, has been successfully saved to the account of Quiz User: '
                            . $QuizUser->getFirstName() . ' ' . $QuizUser->getLastName() . '.';
            $errorMsg   =   FALSE;
        }
        else
        {
            $status     =   FALSE;
            $message    =   'This quiz could not be saved due to the stated errors.';
            $errorMsg   =   $InvalidModels;
        }


        $responseData   =
            [
                'status'        =>  TRUE,
                'actionStatus'  =>  $status,
                'message'       =>  $message,
                'errorMsg'      =>  $errorMsg,
            ];
        return $responseData;
    }


    /**
     * Get saved quiz results for a quiz user identified by customer id
     *
     * @author Chukky Nze <chukky.nze@hyfn.com>
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @example
     *
     * http://vm.avon-scd-api.local/api/v1/get-quiz-results-customer-id
     * ?
     * devKey=b699bf8eadbc24d5c74322c783bb47f9
     * &
     * marketId=174
     * &
     * repPubKey=1e5bc474b25dbd2d40b2b6bf2c99089f
     * &
     * shopperId=1
     * &
     * customerId=1
     * &
     * parentUrl=https://www.google.com
     * &
     * XDEBUG_SESSION_START=PHPSTORM
     *
     *
     * @example     http://vm.avon-scd-api.local/api/v1/get-quiz-results-customer-id?devKey=b699bf8eadbc24d5c74322c783bb47f9&marketId=174&repPubKey=1e5bc474b25dbd2d40b2b6bf2c99089f&shopperId=1&customerId=1&parentUrl=https://www.google.com&XDEBUG_SESSION_START=PHPSTORM
     */
    public function getQuizResultsByCustomerID()
    {
        $QuizUser   =   $this->quizUser
                        ->whereCustomerId(Request::input('customerId'))
                        ->orderBy('updated_at', 'desc')
                        ->first();

        if(isset($QuizUser) && $QuizUser->isValid())
        {
            $results    =   $this->getQuizResults($QuizUser);

            if(count($results) >= 1)
            {
                $status     =   TRUE;
                $message    =   'The Quiz User identified by the Customer ID: ' . Request::input('customerId') . ' successfully returned the included results.';
                $data       =   $results;
            }
            else
            {
                $status     =   TRUE;
                $message    =   'The Quiz User identified by the Customer ID: ' . Request::input('customerId') . ' does not have any results.';
                $data       =   $results;
            }
        }
        else
        {
            $status     =   FALSE;
            $message    =   'The Quiz User identified by the Customer ID: ' . Request::input('customerId') . ' could not be found.';
            $data       =   [];
        }

        return $this::makeResponse
        (
            [
                'status'    =>  $status,
                'message'   =>  $message,
                'data'      =>  $data,
            ],
            200
        );
    }


    /**
     * Get saved quiz results for a quiz user identified by Vkontakte id
     *
     * @author Chukky Nze <chukky.nze@hyfn.com>
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @example     http://vm.avon-scd-api.local/api/v1/get-quiz-results-vkontakte-id?devKey=b699bf8eadbc24d5c74322c783bb47f9&marketId=174&repPubKey=1e5bc474b25dbd2d40b2b6bf2c99089f&shopperId=1&vkontakteId=1&parentUrl=https://www.google.com&XDEBUG_SESSION_START=PHPSTORM
     */
    public function getQuizResultsByVkontakteID()
    {
        $QuizUser   =   $this->quizUser
                        ->whereCustomerId(Request::input('vkontakteId'))
                        ->orderBy('updated_at', 'desc')
                        ->first();

        if(isset($QuizUser) && $QuizUser->isValid())
        {
            $results    =   $this->getQuizResults($QuizUser);

            if(count($results) >= 1)
            {
                $status     =   TRUE;
                $message    =   'The Quiz User identified by the Vkontakte ID: ' . Request::input('vkontakteId') . ' successfully returned the included results.';
                $data       =   $results;
            }
            else
            {
                $status     =   TRUE;
                $message    =   'The Quiz User identified by the Vkontakte ID: ' . Request::input('vkontakteId') . ' does not have any results.';
                $data       =   $results;
            }
        }
        else
        {
            $status     =   FALSE;
            $message    =   'The Quiz User identified by the Vkontakte ID: ' . Request::input('vkontakteId') . ' could not be found.';
            $data       =   [];
        }

        return $this::makeResponse
        (
            [
                'status'    =>  $status,
                'message'   =>  $message,
                'data'      =>  $data,
            ],
            200
        );
    }


    /**
     * Get saved quiz results for a quiz user identified by Facebook id
     *
     * @author Chukky Nze <chukky.nze@hyfn.com>
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @example     http://vm.avon-scd-api.local/api/v1/get-quiz-results-facebook-id?devKey=b699bf8eadbc24d5c74322c783bb47f9&marketId=174&repPubKey=1e5bc474b25dbd2d40b2b6bf2c99089f&shopperId=1&facebookId=1&parentUrl=https://www.google.com&XDEBUG_SESSION_START=PHPSTORM
     */
    public function getQuizResultsByFacebookID()
    {
        $QuizUser   =   $this->quizUser
                        ->whereCustomerId(Request::input('facebookId'))
                        ->orderBy('updated_at', 'desc')
                        ->first();

        if(isset($QuizUser) && $QuizUser->isValid())
        {
            $results    =   $this->getQuizResults($QuizUser);

            if(count($results) >= 1)
            {
                $status     =   TRUE;
                $message    =   'The Quiz User identified by the Facebook ID: ' . Request::input('facebookId') . ' successfully returned the included results.';
                $data       =   $results;
            }
            else
            {
                $status     =   TRUE;
                $message    =   'The Quiz User identified by the Facebook ID: ' . Request::input('facebookId') . ' does not have any results.';
                $data       =   $results;
            }
        }
        else
        {
            $status     =   FALSE;
            $message    =   'The Quiz User identified by the Facebook ID: ' . Request::input('facebookId') . ' could not be found.';
            $data       =   [];
        }

        return $this::makeResponse
        (
            [
                'status'    =>  $status,
                'message'   =>  $message,
                'data'      =>  $data,
            ],
            200
        );
    }


    /**
     * Actual retrieval of the most current QuizResult for the discovered QuizUser
     *
     * @author Chukky Nze <chukky.nze@hyfn.com>
     *
     * @param QuizUser $QuizUser
     * @return array
     */
    public function getQuizResults(QuizUser $QuizUser)
    {
        $QuizResults    =   $this->quizResult->whereQuizUserId($QuizUser->getId())->get();

        $outputArray    =   [];
        foreach ($QuizResults as $quizResult)
        {
            $outputArray[$quizResult->getId()]  =   [
                                                        'id'                =>  $quizResult->getId(),
                                                        'market_id'         =>  $quizResult->getMarketId(),

                                                        'quiz_user_id'      =>  $quizResult->getQuizUserId(),
                                                        'quiz_id'           =>  $quizResult->getQuizId(),

                                                        'question_id'       =>  $quizResult->getQuestionId(),
                                                        'answer_id'         =>  $quizResult->getAnswerId(),
                                                        'product_group_id'  =>  $quizResult->getProductGroupId(),
                                                        'answer_product_id' =>  $quizResult->getAnswerProductId(),

                                                        'was_asked'         =>  $quizResult->isWasAsked(),
                                                        'was_answered'      =>  $quizResult->isWasAnswered(),
                                                        'added_to_cart'     =>  $quizResult->isAddedToCart(),
                                                        'was_bought'        =>  $quizResult->isWasBought(),
                                                        'was_shared'        =>  $quizResult->isWasShared(),
                                                    ];
        }

        return $outputArray;
    }
}