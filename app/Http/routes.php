<?php
Route::get('/', 'WelcomeController@index');

// Route group
Route::group(['prefix' => 'api'], function()
{
    // API Version 1
    Route::group
    (
        [
            'prefix' => 'v1',
            'namespace' => 'Api\\V1'
        ],
        function()
        {
            Route::group(['prefix' => 'proxy', 'middleware' => 'proxy'], function() 
            {
                Route::any('/', function() 
                {
                    return "api";
                });
                Route::get('/addToCart', 'ProxyController@addToCart');
                Route::get('/getCurrentCampaign', 'ProxyController@getCurrentCampaign');

                Route::get('/authenticateCustomer', 'ProxyController@authenticateCustomer');
                Route::get('/getCustomerDetails', 'ProxyController@getCustomerDetails');    
                Route::get('/verifyLogin', 'ProxyController@verifyLogin');            
            });
            Route::group(['middleware' => 'api'], function()
            {   
                Route::any('/', function()
                {
                    return "api";
                });

                Route::get('/init-scd-api'              , 'ScdController@initApiTool');
                Route::get('/init-scd-api-rep-id'       , 'ScdController@initApiTool');
                Route::get('/init-scd-api-rep-pubkey'   , 'ScdController@initApiTool');

                // These should be posts. i know [stop looking at me funny], but Avon and tokens and those deadlines....gotta do what I gotta do
                Route::get('/save-quiz-results-customer-id'    , 'ScdController@saveQuizResultsByCustomerID');
                Route::get('/save-quiz-results-vkontakte-id'   , 'ScdController@saveQuizResultsByVkontakteID');
                Route::get('/save-quiz-results-facebook-id'    , 'ScdController@saveQuizResultsByFacebookID');


                Route::get('/get-quiz-results-customer-id'    , 'ScdController@getQuizResultsByCustomerID');
                Route::get('/get-quiz-results-vkontakte-id'   , 'ScdController@getQuizResultsByVkontakteID');
                Route::get('/get-quiz-results-facebook-id'    , 'ScdController@getQuizResultsByFacebookID');
            });
            
        }
    );

});
