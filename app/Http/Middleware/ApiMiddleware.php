<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\JsonResponse;
use Response;

use App\Libraries\AvonAPI;

class ApiMiddleware
{

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
        $validate       =   AvonAPI::checkRequestInputs($request);

        if(is_array($validate))
        {
            if($validate['status'] && array_key_exists('request', $validate))
            {
                return $next($validate['request']);
            }
            elseif(!$validate['status'] && array_key_exists('response', $validate))
            {
                return AvonAPI::makeAvonResponse($validate['response'], 400);
            }
        }

        if($validate instanceof JsonResponse)
        {
            return $validate;
        }

        return AvonAPI::makeAvonResponse(['I have NO %$#@! idea what you are trying to do! How did you even get here...and what is that you have on?! I\'m going to call Zoolander and the cops.'], 400);
	}

}
