<?php namespace App\Exceptions;

use Exception;
//use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Bugsnag\BugsnagLaravel\BugsnagExceptionHandler as ExceptionHandler;
use \Whoops\Run as WhoopsRun;
use \Whoops\Handler\PrettyPageHandler as WhoopsPrettyPageHandler;
use Illuminate\Http\Response;
use App\Libraries\AvonAPI;

class Handler extends ExceptionHandler {

	/**
	 * A list of the exception types that should not be reported.
	 *
	 * @var array
	 */
	protected $dontReport = [
		'Symfony\Component\HttpKernel\Exception\HttpException'
	];

	/**
	 * Report or log an exception.
	 *
	 * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
	 *
	 * @param  \Exception  $e
	 * @return void
	 */
	public function report(Exception $e)
	{
		return parent::report($e);
	}

	/**
	 * Render an exception into an HTTP response.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Exception  $e
	 * @return \Illuminate\Http\Response
	 */
	public function render($request, Exception $e)
	{
		//return parent::render($request, $e);
		return $this->renderWithWhoops($e);
        /*
        $finalData  =   [
                            'ExceptionData' =>  [
                                                    'code'      =>  $e->getCode(),
                                                    'file'      =>  $e->getFile(),
                                                    'line'      =>  $e->getLine(),
                                                    'message'   =>  $e->getMessage(),
                                                    'stackTrace'=>  $e->getTrace(),
                                                    'previous'  =>  $e->getPrevious(),
                                                ],
                        ];
        return AvonAPI::makeAvonResponse($finalData, 500);
        */
	}

    protected function renderWithWhoops(Exception $e)
    {
        $whoops     =   new WhoopsRun;
        $whoops->pushHandler(new WhoopsPrettyPageHandler);

        return new Response($whoops->handleException($e)->getStatusCode()->getHeaders());
    }
}
